//
//  AccountDetailViewController.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/23.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "AccountDetailViewController.h"
#import "MyWebViewController.h"


#import "SelectSegmentView.h"
#import "AccountDetailTableViewCell.h"



#define SEGHEIGHT 40/HEIGHT_6S_SCALE
#define Cell_H  50.f/HEIGHT_6S_SCALE


@interface AccountDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong) Pager* page;

@property (nonatomic,assign) NSInteger  selectedIndex;

@property (nonatomic,strong)SelectSegmentView *segView;
@property (nonatomic, strong) NSArray *pagesArray;


@end

@implementation AccountDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"余额明细";
    
    self.pagesArray = [NSArray arrayWithObjects:@"收入明细",@"提现记录", nil];
   
    [self creatSubViews];
}

-(void)creatSubViews{
    
    self.selectedIndex = 1;
    
    _segView = [[SelectSegmentView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SEGHEIGHT) titles:self.pagesArray clickBlick:^void(NSInteger index) {
        self.selectedIndex = index;

        self.page.pageIndex = 1;
        [self refreshDataSource];
        
    }];
    _segView.defaultIndex = 1;
    _segView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    _segView.titleSelectColor = getColor(mainColor);
    _segView.lineUnSelectColor = [UIColor whiteColor];
    _segView.titleFont =kFontNameSize(13);
    [self.view addSubview:_segView];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_segView.frame), CGRectGetWidth(self.view.frame),  CGRectGetHeight(self.view.frame) - CGRectGetMaxY(_segView.frame) - NAVGATIONBAR_HEIGHT) style:UITableViewStylePlain];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.view addSubview:self.tableView];
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if (@available(iOS 11.0, *))
    {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.tableView.estimatedRowHeight = 0;
        self.tableView.estimatedSectionFooterHeight = 0;
        self.tableView.estimatedSectionHeaderHeight = 0;
    }

    [self refreshDataSource];
    
}

#pragma mark - Data source
-(void)getData{
    
    NSDictionary* arg;
    
    if (self.selectedIndex == 1) {
        arg = @{
                @"ince":@"get_shop_accountlog",
                @"sid":[UserHandle shareInstance].userId,
                @"page":[WMHelper integerConvertToString:self.page.pageIndex]
                };
    }else{
        arg = @{
                @"ince":@"get_shop_depositlog",
                @"sid":[UserHandle shareInstance].userId,
                @"page":[WMHelper integerConvertToString:self.page.pageIndex]
                };
        
    }
   
    
    [BSDNetTool PostURL:kHost  parameters:arg succeed:^(NSDictionary *data) {
        NSLog(@"余额明细 = %@",data);
        
        if(self.page.pageIndex == 1){
            [self.page.arrayData removeAllObjects];
        }
        NSInteger flag = [[data objectForKey:@"ret"]integerValue];
        if(flag == 200){
            self.page.recordCount = [[data objectForKey:@"total"] integerValue];
            self.page.pageSize = [[data objectForKey:@"number"]integerValue];
            NSInteger empty = self.page.recordCount % self.page.pageSize;
            if(empty == 0){
                self.page.pageCount = self.page.recordCount/self.page.pageSize;
            }else{
                self.page.pageCount = self.page.recordCount/self.page.pageSize+1;
            }
            
            NSMutableArray *tempArr = [AccountModel mj_objectArrayWithKeyValuesArray:[data objectForKey:@"list"]];
            [ self.page.arrayData addObjectsFromArray:tempArr];
            
        }else{
            [self alertHUD:[data objectForKey:@"msg"]];
        }
        [self.tableView reloadData];
        if(self.page.pageCount<=self.page.pageIndex){
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }else{
            [self.tableView.mj_footer endRefreshing];
        }
        if(self.page.pageIndex==1){
            [self.tableView.mj_header endRefreshing];
        }
        
    } failure:^(NSError *error) {
        [self alertHUD:@"网络异常"];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
    
}

-(void)refreshDataSource{
    __weak typeof(self) weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.page.pageIndex = 1;
        [weakSelf getData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.page.pageIndex ++;
        [weakSelf getData];
    }];
    
    [self.tableView.mj_header beginRefreshing];
}


#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.page.arrayData.count;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return Cell_H;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"CELLID";
    
    AccountDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if(!cell){
        cell = [[AccountDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:cellID ];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    AccountModel *model = self.page.arrayData[indexPath.row];
    cell.accountModel = model;
    
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AccountModel *model = self.page.arrayData[indexPath.row];

    if (model.h5_url.length != 0) {
        MyWebViewController *VC = [[MyWebViewController alloc] init];
        VC.webUrlStr = model.h5_url;
        VC.titleName = model.page_title;
        [self.navigationController pushViewController:VC animated:YES];
    }
}



#pragma mark - property package
-(Pager *)page{
    if(!_page){
        _page = [[Pager alloc]init];
        _page.pageIndex = 1;

    }
    return _page;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
