//
//  AccountMessageViewController.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/22.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "AccountMessageViewController.h"
#import "CashApplicationViewController.h"
#import "AccountDetailViewController.h"


@interface AccountMessageViewController ()

@property(nonatomic,strong) UIView* headerView;
@property(nonatomic,strong) UIImageView* iconMoney;
@property(nonatomic,strong) UILabel* labelTitle;
@property(nonatomic,strong) UILabel* labelMoney;
@property(nonatomic,strong) UIButton* btnDeail;
@property(nonatomic,strong) UIButton* btnAppCash;


@property(nonatomic,strong) UILabel*  frozenTitle;
@property(nonatomic,strong) UILabel*  frozenMoney;


@end

@implementation AccountMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.title = @"财务信息";
    [self setUpSubViews];
    [self layoutConstraints];
    
    
    self.frozenTitle.text = self.storeModel.frozen_money_desc;
    self.labelTitle.text = self.storeModel.balanc_desc;
    
    self.labelMoney.text = [NSString stringWithFormat:@"%.2f",[self.storeModel.balance floatValue]];
    self.frozenMoney.text = [NSString stringWithFormat:@"%.2f",[self.storeModel.frozen_money floatValue]];
    
    
}



#pragma mark -
-(void)setUpSubViews{
    [self.view addSubview:self.btnAppCash];
    [self.view addSubview:self.headerView];
    
    [self.headerView addSubview:self.iconMoney];
    [self.headerView addSubview:self.labelTitle];
    [self.headerView addSubview:self.labelMoney];
    [self.headerView addSubview:self.btnDeail];
    
    [self.headerView addSubview:self.frozenMoney];
    [self.headerView addSubview:self.frozenTitle];
    
}

-(void)layoutConstraints{
    
    
    
    [self.btnAppCash mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(50.f);
        make.right.equalTo(self.view);
        make.left.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.right.equalTo(self.view);
        make.left.equalTo(self.view);
        make.bottom.equalTo(self.btnAppCash.mas_top);
    }];
    
    
    
    
    [self.iconMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.equalTo(self.view);
        make.top.mas_equalTo(100.0);
        make.size.mas_equalTo(CGSizeMake(75, 75));
    }];
    
    [self.labelMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.iconMoney.mas_bottom).mas_offset(20);
        make.width.equalTo(self.headerView).multipliedBy(0.5);
        make.left.equalTo(self.headerView);
        make.height.mas_equalTo(40);
        
    }];
    
    
    [self.labelTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelMoney.mas_bottom);
        make.left.right.equalTo(self.labelMoney);
        make.height.equalTo(self.labelMoney);
    }];
    
    
    [self.frozenMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.width.equalTo(self.labelMoney);
        make.left.equalTo(self.labelMoney.mas_right);
        
    }];
    
    
    [self.labelTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelMoney.mas_bottom);
        make.left.right.equalTo(self.labelMoney);
        make.height.equalTo(self.labelMoney);
    }];
    
    [self.frozenTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.width.equalTo(self.labelTitle);
        make.left.equalTo(self.labelTitle.mas_right);
        
    }];
    
    [self.btnDeail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.labelTitle.mas_bottom).mas_offset(20);
        make.left.right.equalTo(self.headerView);
        make.height.equalTo(self.labelTitle);
    }];
    
}


#pragma mark - 点击方法
-(void)balanceBtnDidClick:(UIButton *)sender{

    AccountDetailViewController* controller = [[AccountDetailViewController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)CashApplicationBtnDidClick:(UIButton *)sender{
    
    CashApplicationViewController* controller = [[CashApplicationViewController alloc]init];
    controller.storeModel = self.storeModel;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - 懒加载

-(UIView *)headerView{
    if(!_headerView){
        _headerView = [[UIView alloc]init];
        _headerView.backgroundColor = [UIColor whiteColor];
        
    }
    return _headerView;
}

-(UIImageView *)iconMoney{
    if(!_iconMoney){
        _iconMoney = [[UIImageView alloc]init];
        [_iconMoney setImage:[UIImage imageNamed:@"icon-cash"]];
    }
    return _iconMoney;
}

-(UILabel *)labelTitle{
    if(!_labelTitle){
        _labelTitle = [[UILabel alloc]init];
        _labelTitle.text = @"账户余额(元)";
        _labelTitle.textAlignment = NSTextAlignmentCenter;
        _labelTitle.textColor = [UIColor colorWithRed:156/255.f green:156/244.f blue:156/255.f alpha:1.0];
        _labelTitle.font = [UIFont systemFontOfSize:15.f];
    }
    return _labelTitle;
}

-(UILabel *)frozenTitle{
    if(!_frozenTitle){
        _frozenTitle = [[UILabel alloc]init];
        _frozenTitle.text = @"冻结金额(元)";
        _frozenTitle.textAlignment = NSTextAlignmentCenter;
        _frozenTitle.textColor = [UIColor colorWithRed:156/255.f green:156/244.f blue:156/255.f alpha:1.0];
        _frozenTitle.font = [UIFont systemFontOfSize:15.f];
    }
    return _frozenTitle;
    
}

- (UILabel *)frozenMoney{
    
    if(!_frozenMoney){
        _frozenMoney = [[UILabel alloc]init];
        _frozenMoney.textAlignment = NSTextAlignmentCenter;
        _frozenMoney.textColor = [UIColor colorWithRed:57/255.f green:174/244.f blue:222/255.f alpha:1.0];
        _frozenMoney.font = [UIFont boldSystemFontOfSize:24.f];
    }
    return _frozenMoney;
}

-(UILabel *)labelMoney{
    if(!_labelMoney){
        _labelMoney = [[UILabel alloc]init];
        _labelMoney.textAlignment = NSTextAlignmentCenter;
        _labelMoney.textColor = [UIColor colorWithRed:57/255.f green:174/244.f blue:222/255.f alpha:1.0];
        _labelMoney.font = [UIFont boldSystemFontOfSize:24.f];
    }
    return _labelMoney;
}

-(UIButton *)btnDeail{
    if(!_btnDeail){
        _btnDeail = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnDeail setTitleColor:[UIColor colorWithRed:57/255.f green:174/244.f blue:222/255.f alpha:1.0] forState:UIControlStateNormal];
        _btnDeail.titleLabel.font =[UIFont systemFontOfSize:17.f];
        [_btnDeail setTitle:@"查看余额明细" forState:UIControlStateNormal];
        _btnDeail.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [_btnDeail addTarget:self action:@selector(balanceBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnDeail;
}

-(UIButton *)btnAppCash{
    if(!_btnAppCash){
        _btnAppCash = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnAppCash setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnAppCash setTitle:@"申请提现" forState:UIControlStateNormal];
        _btnAppCash.titleLabel.font =[UIFont systemFontOfSize:17.f];
        _btnAppCash.backgroundColor = getColor(mainColor);
        [_btnAppCash addTarget:self action:@selector(CashApplicationBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _btnAppCash;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
