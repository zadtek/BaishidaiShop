//
//  MySettingViewController.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/23.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "MySettingViewController.h"
#import "JPUSHService.h"


#define FooterView_Height    10/HEIGHT_6S_SCALE
#define FooterButton_Height    90/HEIGHT_6S_SCALE
#define Cell_H    50/HEIGHT_6S_SCALE



@interface MySettingViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)UITableView *tableView;

@property (nonatomic,strong)UIButton *loginoutBtn;

@property (nonatomic,strong)UILabel *versionLabel;


@end

@implementation MySettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title =  @"设置";

    [self creatTableView];

    
}
-(void)creatTableView{
    
    float table_h = SCREEN_HEIGHT - NAVGATIONBAR_HEIGHT;
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, table_h) style:UITableViewStyleGrouped];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.tableView];
    self.tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.tableFooterView = [self creatFooterView];
    
    if (@available(iOS 11.0, *))
    {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.tableView.estimatedRowHeight = 0;
        self.tableView.estimatedSectionFooterHeight = 0;
        self.tableView.estimatedSectionHeaderHeight = 0;
    }
    
    [self initTalbeViewSubview];
    
}

-(void)initTalbeViewSubview{
    
    float textWidth =  250/WIDTH_6S_SCALE;
    float textX = SCREEN_WIDTH - textWidth - 10;
    float textHeigth = Cell_H;
    
    
    _versionLabel = [[UILabel alloc] initWithFrame:CGRectMake(textX, 0, textWidth, textHeigth)];
    [_versionLabel setFont:kFontNameSize(17)];
    _versionLabel.textAlignment = NSTextAlignmentRight;
    
    
    NSDictionary *bundleDic = [[NSBundle mainBundle] infoDictionary];
    NSString* releaseVersion = [bundleDic objectForKey:@"CFBundleShortVersionString"];
    _versionLabel.text = [NSString stringWithFormat:@"V%@",releaseVersion];
    
}

#pragma mark 提交按钮
-(UIView *)creatFooterView{
    
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, FooterButton_Height)];
    
    self.loginoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.loginoutBtn addTarget:self action:@selector(loginoutBtnAction) forControlEvents:UIControlEventTouchUpInside];
    self.loginoutBtn.layer.masksToBounds = YES;
    self.loginoutBtn.layer.cornerRadius = 5;
    self.loginoutBtn.titleLabel.font= kFontNameSize(16);
    [footerView addSubview:self.loginoutBtn];
    [self.loginoutBtn setTitle:@"退出登录" forState:UIControlStateNormal];
    self.loginoutBtn.backgroundColor=  getColor(mainColor);
    [self.loginoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.loginoutBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(FooterButton_Height/2);
        make.left.mas_equalTo(footerView).mas_offset(20);
        make.right.mas_equalTo(footerView).mas_offset(-20);
        make.centerY.mas_equalTo(footerView);
    }];
    
    
    return footerView;
}

-(void)loginoutBtnAction{
    
    UIAlertController *alt = [UIAlertController alertControllerWithTitle:@"确认退出吗?" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        //退出登陆成功，设置用户为未登陆状态
        [[UserHandle shareInstance] setloginState:NO];
        [[UserHandle shareInstance] clearAllUserDefaultsData];
        [[UserHandle shareInstance] synchronize];
        [self logoutJPush];
        
        [[SDImageCache sharedImageCache] clearMemory];
        [[SDImageCache sharedImageCache] clearDiskOnCompletion:^{
            
            MainNavgationViewController *navVc = [[MainNavgationViewController alloc] initWithRootViewController:[[LoginViewController alloc] init]];
            [UIApplication sharedApplication].keyWindow.rootViewController = navVc;
            
            
        }];
        
    }];
    
    
    [alt addAction:cancelAction];
    [alt addAction:okAction];
    [self presentViewController:alt animated:YES completion:nil];
    
}


#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

//tableView代理方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 1;
    
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"CELLID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:cellID];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.text = @"版本号";
    
    cell.accessoryView = _versionLabel;
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return Cell_H;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    
}


- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerView = [[UIView alloc] init];
    return headerView;
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return FooterView_Height;
    
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *footerView = [[UIView alloc] init];
    return footerView;
    
}
#pragma mark - 推送退出
-(void)logoutJPush{
    
    [JPUSHService deleteAlias:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
        NSLog(@"消息推送退出%tu,%@,%tu",iResCode,iAlias,seq);
        if(iResCode == 0){
            
        }else{
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"消息推送退出失败!" preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertController animated:YES completion:nil];
            
        }
    } seq:1];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
