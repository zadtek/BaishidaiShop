//
//  PushGoodsViewController.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/22.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "PushGoodsViewController.h"
#import "TZImagePickerController.h"
#import "UIView+Layout.h"
#import "UpdatePhotoCollectionViewCell.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "TZImageManager.h"
#import "GoodsModel.h"


#define maxCount  4


#define FooterButton_Height    80/HEIGHT_6S_SCALE
#define HeaderView_Height    50/HEIGHT_6S_SCALE
#define Space_X    15/WIDTH_6S_SCALE
#define PhotoView_H    130/HEIGHT_6S_SCALE



@interface PushGoodsViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,TZImagePickerControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    NSMutableArray *_selectedPhotos;
    NSMutableArray *_selectedAssets;
    BOOL _isSelectOriginalPhoto;
    
    CGFloat _itemWH;
    CGFloat _margin;
}
@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,strong)NSMutableArray *platformArray;
@property(nonatomic,strong)NSMutableArray *storeArray;

@property(nonatomic,strong)UIView *photoView;
@property(nonatomic,strong)UIButton *platformBtn;
@property(nonatomic,strong)UIButton *storeBtn;
@property(nonatomic,strong)UITextField *nameTF;
@property(nonatomic,strong)UITextField *priceTF;
@property(nonatomic,strong)UIButton *commitbtn;


@property (nonatomic, copy) NSString *typeId;
@property (nonatomic, copy) NSString *platformId;

@property (nonatomic, strong) UIImagePickerController *imagePickerVc;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (strong, nonatomic) CLLocation *location;

@end

@implementation PushGoodsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"发布商品";
    
    self.platformArray = [NSMutableArray array];
    self.storeArray = [NSMutableArray array];
    
    [self querySelectData];
    
    
}
-(void)creatSubViews{
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,  CGRectGetHeight(self.view.frame) ) style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
    self.tableView.tableFooterView = [self creatFooterView];
    [self.view addSubview:self.tableView];
    
    if (@available(iOS 11.0, *))
    {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.tableView.estimatedRowHeight = 0;
        self.tableView.estimatedSectionFooterHeight = 0;
        self.tableView.estimatedSectionHeaderHeight = 0;
    }
    
    [self initTalbeViewSubview];
}

-(void)querySelectData{
    
    [self showHUD];
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        NSDictionary* arg = @{@"ince":@"getshopcat",
                              @"sid":[UserHandle shareInstance].userId,
                              @"parent_id":@"0"
                              };
        
        [BSDNetTool PostURL:kHost  parameters:arg succeed:^(NSDictionary *data) {
            NSLog(@"data = %@",data);
            NSInteger flag = [[data objectForKey:@"ret"]integerValue];
            if(flag == 200){
                self.storeArray = [GoodsModel mj_objectArrayWithKeyValuesArray:[data objectForKey:@"data"]];

            }
            
        } failure:^(NSError *error) {
            [self hidHUD:@"网络异常"];
        }];
        dispatch_group_leave(group);
    });
    dispatch_group_enter(group);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        NSDictionary* arg = @{@"ince":@"get_category",
                              @"sid":[UserHandle shareInstance].userId,
                              };
        [BSDNetTool PostURL:kHost  parameters:arg succeed:^(NSDictionary *data) {
            NSLog(@"data = %@",data);
            NSInteger flag = [[data objectForKey:@"ret"]integerValue];
            if(flag == 200){
                self.platformArray = [NSMutableArray arrayWithArray:data[@"data"]];
            }
            
        } failure:^(NSError *error) {
            [self hidHUD:@"网络异常"];
        }];
        dispatch_group_leave(group);
    });
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        
        [self hidHUD];
        [self creatSubViews];
        
    });
    
}


-(void)initTalbeViewSubview{
    
    float textWidth =  250/WIDTH_6S_SCALE;
    float textX = SCREEN_WIDTH - textWidth - Space_X;
    float textHeigth = HeaderView_Height;
    
    
    _nameTF = [[UITextField alloc] initWithFrame:CGRectMake(textX, 0, textWidth, textHeigth)];
    _nameTF.returnKeyType = UIReturnKeyDone;
    [_nameTF setFont:kFontNameSize(14)];
    _nameTF.placeholder=@"请输入商品名称";
    _nameTF.delegate = self;
    _nameTF.textAlignment = NSTextAlignmentRight;
    _nameTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    _priceTF = [[UITextField alloc] initWithFrame:CGRectMake(textX, 0, textWidth, textHeigth)];
    [_priceTF setFont:kFontNameSize(14)];
    _priceTF.placeholder=@"请输入商品价格";
    _priceTF.delegate = self;
    _priceTF.textAlignment = NSTextAlignmentRight;
    _priceTF.keyboardType = UIKeyboardTypeNumberPad;
    _priceTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    
    
    _platformBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _platformBtn.titleLabel.numberOfLines = 2;
    _platformBtn.frame = CGRectMake(textX, 0, textWidth, textHeigth);
    _platformBtn.titleLabel.font = kFontNameSize(14);
    [_platformBtn setTitle:@"请选择平台分类 >" forState:UIControlStateNormal];
    [_platformBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    _platformBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [_platformBtn addTarget:self action:@selector(chooseButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _storeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _storeBtn.frame = CGRectMake(textX, 0, textWidth, textHeigth);
    _storeBtn.titleLabel.font = kFontNameSize(14);
    [_storeBtn setTitle:@"请选择店铺分类 >" forState:UIControlStateNormal];
    [_storeBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    _storeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [_storeBtn addTarget:self action:@selector(chooseButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    _platformBtn.tag = 10001;
    _storeBtn.tag = 10002;
    
    _photoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), PhotoView_H)];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake( Space_X, 0, SCREEN_WIDTH - Space_X , 30/HEIGHT_6S_SCALE)];
    titleLabel.text = @"图片(1-4张)";
    titleLabel.font = kFontNameSize(14);
    titleLabel.textColor = getColor(blackColor);
    [_photoView addSubview:titleLabel];
    

    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(titleLabel.frame), CGRectGetWidth(self.view.frame), PhotoView_H - CGRectGetMaxY(titleLabel.frame)) collectionViewLayout:layout];
    _collectionView.backgroundColor = [UIColor clearColor];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    _collectionView.showsVerticalScrollIndicator = NO;
    _collectionView.showsHorizontalScrollIndicator = NO;
    _collectionView.scrollEnabled = NO;
    _collectionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [_collectionView registerClass:[UpdatePhotoCollectionViewCell class] forCellWithReuseIdentifier:@"UpdatePhotoCollectionViewCell"];
    [_photoView addSubview:_collectionView];
    
    
    _itemWH = 60/HEIGHT_6S_SCALE;
    _margin = (CGRectGetHeight(_collectionView.frame) - _itemWH)/2;
    
}
#pragma mark 提交按钮
-(UIView *)creatFooterView{
    
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, FooterButton_Height)];
    
    self.commitbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.commitbtn addTarget:self action:@selector(commitBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    self.commitbtn.layer.masksToBounds = YES;
    self.commitbtn.layer.cornerRadius = 10;
    self.commitbtn.titleLabel.font=kFontNameSize(16);
    [footerView addSubview:self.commitbtn];
    [self.commitbtn setTitle:@"发布商品" forState:UIControlStateNormal];
    self.commitbtn.backgroundColor=  getColor(greenBackColor);
    [self.commitbtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.commitbtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(FooterButton_Height/2);
        make.left.mas_equalTo(footerView).mas_offset(40);
        make.right.mas_equalTo(footerView).mas_offset(-40);
        make.centerY.mas_equalTo(footerView);
        
    }];
    
    
    return footerView;
}

#pragma mark - 点击区域 标签
- (void)chooseButtonClicked:(UIButton *)chooseButton{
    
    [self.view endEditing:YES];
    
    __weak typeof(self) weakSelf = self;
    
    if (chooseButton == self.platformBtn) {
        
        [CGXPickerView showAddressPickerWithTitle:@"平台分类" DataSource:self.platformArray DefaultSelected:@[@0, @0,@0] IsAutoSelect:NO Manager:nil ResultBlock:^(NSArray *selectAddressArr, NSArray *selectAddressRow,NSString *code) {
        
            NSLog(@"code = %@",code);
            
            self.platformId = code;
            NSString *textStr = [NSString stringWithFormat:@"%@-%@-%@", selectAddressArr[0], selectAddressArr[1],selectAddressArr[2]];
            
            [weakSelf.platformBtn setTitle:textStr forState:UIControlStateNormal];
            [weakSelf.platformBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
        }];
        
    }
    
    if (chooseButton == self.storeBtn) {
        
        NSMutableArray *array = [NSMutableArray array];
        for (GoodsModel *model in self.storeArray) {
            [array addObject:model.cat_name];
        }
        [CGXPickerView showStringPickerWithTitle:@"店铺分类" DataSource:array DefaultSelValue:array.firstObject IsAutoSelect:NO Manager:nil ResultBlock:^(id selectValue, id selectRow) {
            
            NSInteger index = [selectRow integerValue];
            GoodsModel *model = self.storeArray[index];
            self.typeId = [NSString stringWithFormat:@"%@",model.cat_id];
            [self.storeBtn setTitle:[NSString stringWithFormat:@"%@",selectValue] forState:UIControlStateNormal];
            [self.storeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            NSLog(@"typeId = %@",self.typeId);
            
            
        }];
        
    }
    
    
}


#pragma mark -  UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

//tableView代理方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"CELL_ID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = kFontNameSize(14);
 
    if (cell.contentView.subviews.count > 0) {
        
        for (UIView *views in cell.contentView.subviews) {
            
            [views removeFromSuperview];
        }
    }
    
    
    if (indexPath.row == 0) {
        
        cell.textLabel.text = @"商品名称";
        cell.accessoryView = _nameTF;
        
    }else if (indexPath.row == 1){
        
        cell.textLabel.text = @"平台分类";
        cell.accessoryView = _platformBtn;
        
    }else if (indexPath.row == 2){
        
        cell.textLabel.text = @"店铺分类";
        cell.accessoryView = _storeBtn;
        
    }else if (indexPath.row == 3){
        
        cell.textLabel.text = @"价格";
        cell.accessoryView = _priceTF;
        
    }else{
        
        [cell.contentView addSubview:self.photoView];
    }
    
    return cell;
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 4 ) {
        return PhotoView_H;
    }else{
        return HeaderView_Height;
        
    }
}



#pragma mark - 点击提交评价按钮
-(void)commitBtnAction:(UIButton *)button{
    
    if (self.nameTF.text.length == 0) {
        ShowMessage(@"请输入商品名称");
        return;
    }
    if ([_platformBtn.titleLabel.text rangeOfString:@"选择"].location != NSNotFound) {
        ShowMessage(@"请选择平台分类");
        return;
    }
    if ([_storeBtn.titleLabel.text rangeOfString:@"选择"].location != NSNotFound) {
        ShowMessage(@"请选择店铺分类");
        return;
    }
    
    
    if (self.priceTF.text.length == 0) {
        ShowMessage(@"请输入商品价格");
        return;
    }
    
    if (_selectedPhotos.count == 0) {
        [self commitDataWithOriginalArray:nil UrlArray:nil GoodsArray:nil];
        
    }else{
        [self commitChooseImageData];
        
    }
    
    
    
    
    
}
-(void)commitChooseImageData{
    
    [self showHUD];
    NSMutableArray *originalArray = [NSMutableArray array];
    NSMutableArray *urlArray = [NSMutableArray array];
    NSMutableArray *goodsArray = [NSMutableArray array];
    
    for (int i = 0; i < _selectedPhotos.count ; i++) {
        
        UIImage *image = _selectedPhotos[i];
        NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
        
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setValue:@"arr" forKey:@"ince"];
        [dic setValue:[UserHandle shareInstance].userId forKey:@"sid"];
        
        [BSDNetTool requestAFURL:kHost parameters:dic imageData:imageData succeed:^(NSDictionary *data) {
            [self hidHUD];
            NSInteger flag = [[data objectForKey:@"ret"]integerValue];
            if(flag == 200){
                
                NSString *original = data[@"original_img"];
                NSString *url = data[@"url"];
                NSString *goods = data[@"goods_img"];
                
                
                [originalArray addObject:original];
                [urlArray addObject:url];
                [goodsArray addObject:goods];
                
                if (i == _selectedPhotos.count -1) {
                    [self commitDataWithOriginalArray:originalArray UrlArray:urlArray GoodsArray:goodsArray];
                }
                
            }
            
        } failure:^(NSError *error) {
            [self hidHUD:@"网络异常"];
        }];
        
        
    }
    
}
-(void)commitDataWithOriginalArray:(NSArray *)originalArray UrlArray:(NSArray *)urlArray GoodsArray:(NSArray *)goodsArray{

    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@"add_goods" forKey:@"ince"];
    [dic setValue:[UserHandle shareInstance].userId forKey:@"sid"];
    
    [dic setValue:self.nameTF.text forKey:@"goods_name"];
    [dic setValue:self.priceTF.text forKey:@"price"];
    
    [dic setValue:self.platformId forKey:@"cat_id"];
    [dic setValue:self.typeId forKey:@"user_cat_id"];
    
    
    if (originalArray.count != 0) {
        NSString *original = [originalArray componentsJoinedByString:@","];
        [dic setValue:original forKey:@"original_img"];
        NSString *url = [urlArray componentsJoinedByString:@","];
        [dic setValue:url forKey:@"goods_thumb"];
        NSString *goods = [goodsArray componentsJoinedByString:@","];
        [dic setValue:goods forKey:@"goods_img"];
        
    }else{
        [dic setValue:@"" forKey:@"original_img"];
        [dic setValue:@"" forKey:@"goods_thumb"];
        [dic setValue:@"" forKey:@"goods_img"];
        
    }
    
    [self showHUD];
    [BSDNetTool PostURL:kHost  parameters:dic succeed:^(NSDictionary *data) {
        [self hidHUD];
        NSLog(@"data = %@",data);
        NSInteger flag = [[data objectForKey:@"ret"]integerValue];
        if(flag == 200){
            
            ShowMessage(data[@"msg"]);
            [self performSelector:@selector(popView) withObject:nil afterDelay:1.5];
            
        }else{
            ShowMessage(data[@"msg"]);
        }
        
    } failure:^(NSError *error) {
        [self hidHUD:@"网络异常"];
    }];
    
    
}
-(void)popView{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -  UITextField
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


//限制输入
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == self.priceTF) {
        if (string.length == 0){
            return YES;
        }else{
            NSInteger existedLength = textField.text.length;
            NSInteger selectedLength = range.length;
            NSInteger replaceLength = string.length;
            if (existedLength - selectedLength + replaceLength > 11) {
                return NO;
            }
        }
        
    }
    
    
    NSMutableString *str = [[NSMutableString alloc] initWithString:textField.text];
    [str insertString:string atIndex:range.location];
    
    if (textField == self.priceTF) {
        
        if (string.length != 0 ){
            
            //限制输入
            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
            NSString*filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            BOOL basicTest = [string isEqualToString:filtered];
            if (!basicTest) {
                return NO;
            }else{
                return YES;
            }
            
        }
    }
    
    return YES;
}
#pragma mark -  UIImagePickerController
- (UIImagePickerController *)imagePickerVc {
    if (_imagePickerVc == nil) {
        _imagePickerVc = [[UIImagePickerController alloc] init];
        _imagePickerVc.delegate = self;
        // set appearance / 改变相册选择页的导航栏外观
        if (iOS7Later) {
            _imagePickerVc.navigationBar.barTintColor = self.navigationController.navigationBar.barTintColor;
        }
        _imagePickerVc.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
        UIBarButtonItem *tzBarItem, *BarItem;
        if (@available(iOS 9, *)) {
            tzBarItem = [UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[TZImagePickerController class]]];
            BarItem = [UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UIImagePickerController class]]];
        } else {
            tzBarItem = [UIBarButtonItem appearanceWhenContainedIn:[TZImagePickerController class], nil];
            BarItem = [UIBarButtonItem appearanceWhenContainedIn:[UIImagePickerController class], nil];
        }
        NSDictionary *titleTextAttributes = [tzBarItem titleTextAttributesForState:UIControlStateNormal];
        [BarItem setTitleTextAttributes:titleTextAttributes forState:UIControlStateNormal];
        
    }
    return _imagePickerVc;
}
#pragma mark UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (_selectedPhotos.count >= maxCount) {
        return _selectedPhotos.count;
    }
    for (PHAsset *asset in _selectedAssets) {
        if (asset.mediaType == PHAssetMediaTypeVideo) {
            return _selectedPhotos.count;
        }
    }
    
    return _selectedPhotos.count + 1;
}
#pragma mark -- UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(_itemWH, _itemWH);
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(_margin, _margin, _margin, _margin);;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return _margin;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return _margin;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UpdatePhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UpdatePhotoCollectionViewCell" forIndexPath:indexPath];
    cell.videoImageView.hidden = YES;
    if (indexPath.item == _selectedPhotos.count) {
        cell.imageView.image = [UIImage imageNamed:@"AlbumAddBtn"];
        cell.deleteBtn.hidden = YES;
        cell.gifLable.hidden = YES;
    } else {
        cell.imageView.image = _selectedPhotos[indexPath.item];
        cell.asset = _selectedAssets[indexPath.item];
        cell.deleteBtn.hidden = NO;
    }
    
    cell.deleteBtn.tag = indexPath.item;
    [cell.deleteBtn addTarget:self action:@selector(deleteBtnClik:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item == _selectedPhotos.count) {
       
        [self pushTZImagePickerController];

    } else { // preview photos or video / 预览照片或者视频
        id asset = _selectedAssets[indexPath.item];
        BOOL isVideo = NO;
        if ([asset isKindOfClass:[PHAsset class]]) {
            PHAsset *phAsset = asset;
            isVideo = phAsset.mediaType == PHAssetMediaTypeVideo;
        }
        
        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithSelectedAssets:_selectedAssets selectedPhotos:_selectedPhotos index:indexPath.item];
        imagePickerVc.maxImagesCount = maxCount;
        imagePickerVc.allowPickingMultipleVideo = NO;
        imagePickerVc.showSelectedIndex = YES;
        imagePickerVc.allowPickingOriginalPhoto = NO;
        imagePickerVc.allowPickingGif = NO;
        
        
        imagePickerVc.isSelectOriginalPhoto = _isSelectOriginalPhoto;
        [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
            self->_selectedPhotos = [NSMutableArray arrayWithArray:photos];
            self->_selectedAssets = [NSMutableArray arrayWithArray:assets];
            self->_isSelectOriginalPhoto = isSelectOriginalPhoto;
            [self->_collectionView reloadData];
            self->_collectionView.contentSize = CGSizeMake(0, ((self->_selectedPhotos.count + 2) / 3 ) * (self->_margin + self->_itemWH));
        }];
        [self presentViewController:imagePickerVc animated:YES completion:nil];
        //        }
    }
}


#pragma mark - TZImagePickerController

- (void)pushTZImagePickerController {
    if (maxCount <= 0) {
        return;
    }
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:maxCount columnNumber:maxCount delegate:self pushPhotoPickerVc:YES];
    // imagePickerVc.navigationBar.translucent = NO;
    
#pragma mark - 五类个性化设置，这些参数都可以不传，此时会走默认设置
    imagePickerVc.isSelectOriginalPhoto = _isSelectOriginalPhoto;
    
    if (maxCount > 1) {
        // 1.设置目前已经选中的图片数组
        imagePickerVc.selectedAssets = _selectedAssets; // 目前已经选中的图片数组
    }
    imagePickerVc.allowTakePicture = YES; // 在内部显示拍照按钮
    imagePickerVc.allowTakeVideo = NO;   // 在内部显示拍视频按
    imagePickerVc.videoMaximumDuration = 10; // 视频最大拍摄时间
    [imagePickerVc setUiImagePickerControllerSettingBlock:^(UIImagePickerController *imagePickerController) {
        imagePickerController.videoQuality = UIImagePickerControllerQualityTypeHigh;
    }];
    
    imagePickerVc.iconThemeColor = [UIColor colorWithRed:31 / 255.0 green:185 / 255.0 blue:34 / 255.0 alpha:1.0];
    imagePickerVc.showPhotoCannotSelectLayer = YES;
    imagePickerVc.cannotSelectLayerColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    [imagePickerVc setPhotoPickerPageUIConfigBlock:^(UICollectionView *collectionView, UIView *bottomToolBar, UIButton *previewButton, UIButton *originalPhotoButton, UILabel *originalPhotoLabel, UIButton *doneButton, UIImageView *numberImageView, UILabel *numberLabel, UIView *divideLine) {
        [doneButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    }];

    // 3. Set allow picking video & photo & originalPhoto or not
    // 3. 设置是否可以选择视频/图片/原图
    imagePickerVc.allowPickingVideo =NO;
    imagePickerVc.allowPickingImage = YES;
    imagePickerVc.allowPickingOriginalPhoto = YES;
    imagePickerVc.allowPickingGif = NO;
    imagePickerVc.allowPickingMultipleVideo = NO; // 是否可以多选视频
    
    // 4. 照片排列按修改时间升序
    imagePickerVc.sortAscendingByModificationDate = YES;
    
    /// 5. 单选模式,maxImagesCount为1时才生效
    imagePickerVc.showSelectBtn = NO;
    imagePickerVc.allowCrop = YES;
    imagePickerVc.needCircleCrop = NO;
    // 设置竖屏下的裁剪尺寸
    NSInteger left = 30;
    NSInteger widthHeight = SCREEN_WIDTH - 2 * left;
    NSInteger top = (SCREEN_HEIGHT - widthHeight) / 2;
    imagePickerVc.cropRect = CGRectMake(left, top, widthHeight, widthHeight);

    imagePickerVc.statusBarStyle = UIStatusBarStyleLightContent;
    
    // 设置是否显示图片序号
    imagePickerVc.showSelectedIndex = YES;
    
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        
    }];
    
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}


#pragma mark - UIImagePickerController

- (void)takePhoto {
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if ((authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied) && iOS7Later) {
        // 无相机权限 做一个友好的提示
        if (iOS8Later) {
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"无法使用相机" message:@"请在iPhone的""设置-隐私-相机""中允许访问相机" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"设置", nil];
            [alert show];
        } else {
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"无法使用相机" message:@"请在iPhone的""设置-隐私-相机""中允许访问相机" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
        }
    } else if (authStatus == AVAuthorizationStatusNotDetermined) {
        // fix issue 466, 防止用户首次拍照拒绝授权时相机页黑屏
        if (iOS7Later) {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if (granted) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self takePhoto];
                    });
                }
            }];
        } else {
            [self takePhoto];
        }
        // 拍照之前还需要检查相册权限
    } else if ([TZImageManager authorizationStatus] == 2) { // 已被拒绝，没有相册权限，将无法保存拍的照片
        if (iOS8Later) {
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"无法访问相册" message:@"请在iPhone的""设置-隐私-相册""中允许访问相册" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"设置", nil];
            [alert show];
        } else {
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"无法访问相册" message:@"请在iPhone的""设置-隐私-相册""中允许访问相册" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
        }
    } else if ([TZImageManager authorizationStatus] == 0) { // 未请求过相册权限
        [[TZImageManager manager] requestAuthorizationWithCompletion:^{
            [self takePhoto];
        }];
    } else {
        [self pushImagePickerController];
    }
}

// 调用相机
- (void)pushImagePickerController {
    // 提前定位
    __weak typeof(self) weakSelf = self;
    [[TZLocationManager manager] startLocationWithSuccessBlock:^(NSArray<CLLocation *> *locations) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        strongSelf.location = [locations firstObject];
    } failureBlock:^(NSError *error) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        strongSelf.location = nil;
    }];
    
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
        self.imagePickerVc.sourceType = sourceType;
        NSMutableArray *mediaTypes = [NSMutableArray array];

        [mediaTypes addObject:(NSString *)kUTTypeImage];

        if (mediaTypes.count) {
            _imagePickerVc.mediaTypes = mediaTypes;
        }
        if (iOS8Later) {
            _imagePickerVc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        }
        [self presentViewController:_imagePickerVc animated:YES completion:nil];
    } else {
        NSLog(@"模拟器中无法打开照相机,请在真机中使用");
    }
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) { // take photo / 去拍照
        [self takePhoto];
    } else if (buttonIndex == 1) {
        [self pushTZImagePickerController];
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) { // 去设置界面，开启相机访问权限
        if (iOS8Later) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
    }
}

#pragma mark - TZImagePickerControllerDelegate

/// User click cancel button
/// 用户点击了取消
- (void)tz_imagePickerControllerDidCancel:(TZImagePickerController *)picker {
    // NSLog(@"cancel");
}

// The picker should dismiss itself; when it dismissed these handle will be called.
// If isOriginalPhoto is YES, user picked the original photo.
// You can get original photo with asset, by the method [[TZImageManager manager] getOriginalPhotoWithAsset:completion:].
// The UIImage Object in photos default width is 828px, you can set it by photoWidth property.
// 这个照片选择器会自己dismiss，当选择器dismiss的时候，会执行下面的代理方法
// 如果isSelectOriginalPhoto为YES，表明用户选择了原图
// 你可以通过一个asset获得原图，通过这个方法：[[TZImageManager manager] getOriginalPhotoWithAsset:completion:]
// photos数组里的UIImage对象，默认是828像素宽，你可以通过设置photoWidth属性的值来改变它
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto infos:(NSArray<NSDictionary *> *)infos {
    _selectedPhotos = [NSMutableArray arrayWithArray:photos];
    _selectedAssets = [NSMutableArray arrayWithArray:assets];
    _isSelectOriginalPhoto = isSelectOriginalPhoto;
    [_collectionView reloadData];
    // _collectionView.contentSize = CGSizeMake(0, ((_selectedPhotos.count + 2) / 3 ) * (_margin + _itemWH));
    
    // 1.打印图片名字
    [self printAssetsName:assets];
    // 2.图片位置信息
    if (iOS8Later) {
        for (PHAsset *phAsset in assets) {
            NSLog(@"location:%@",phAsset.location);
        }
    }
    
}

// If user picking a video, this callback will be called.
// If system version > iOS8,asset is kind of PHAsset class, else is ALAsset class.
// 如果用户选择了一个视频，下面的handle会被执行
// 如果系统版本大于iOS8，asset是PHAsset类的对象，否则是ALAsset类的对象
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingVideo:(UIImage *)coverImage sourceAssets:(id)asset {
    _selectedPhotos = [NSMutableArray arrayWithArray:@[coverImage]];
    _selectedAssets = [NSMutableArray arrayWithArray:@[asset]];
    // open this code to send video / 打开这段代码发送视频
    [[TZImageManager manager] getVideoOutputPathWithAsset:asset presetName:AVAssetExportPreset640x480 success:^(NSString *outputPath) {
        NSLog(@"视频导出到本地完成,沙盒路径为:%@",outputPath);
        // Export completed, send video here, send by outputPath or NSData
        // 导出完成，在这里写上传代码，通过路径或者通过NSData上传
    } failure:^(NSString *errorMessage, NSError *error) {
        NSLog(@"视频导出失败:%@,error:%@",errorMessage, error);
    }];
    [_collectionView reloadData];
    // _collectionView.contentSize = CGSizeMake(0, ((_selectedPhotos.count + 2) / 3 ) * (_margin + _itemWH));
}

// If user picking a gif image, this callback will be called.
// 如果用户选择了一个gif图片，下面的handle会被执行
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingGifImage:(UIImage *)animatedImage sourceAssets:(id)asset {
    _selectedPhotos = [NSMutableArray arrayWithArray:@[animatedImage]];
    _selectedAssets = [NSMutableArray arrayWithArray:@[asset]];
    [_collectionView reloadData];
}

// Decide album show or not't
// 决定相册显示与否
- (BOOL)isAlbumCanSelect:(NSString *)albumName result:(id)result {
    
    return YES;
}

// Decide asset show or not't
// 决定asset显示与否
- (BOOL)isAssetCanSelect:(id)asset {
    
    return YES;
}

#pragma mark - Click Event

- (void)deleteBtnClik:(UIButton *)sender {
    if ([self collectionView:self.collectionView numberOfItemsInSection:0] <= _selectedPhotos.count) {
        [_selectedPhotos removeObjectAtIndex:sender.tag];
        [_selectedAssets removeObjectAtIndex:sender.tag];
        [self.collectionView reloadData];
        return;
    }
    
    [_selectedPhotos removeObjectAtIndex:sender.tag];
    [_selectedAssets removeObjectAtIndex:sender.tag];
    [_collectionView performBatchUpdates:^{
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:sender.tag inSection:0];
        [self->_collectionView deleteItemsAtIndexPaths:@[indexPath]];
    } completion:^(BOOL finished) {
        [self->_collectionView reloadData];
    }];
}


#pragma mark - Private

/// 打印图片名字
- (void)printAssetsName:(NSArray *)assets {
    NSString *fileName;
    for (id asset in assets) {
        if ([asset isKindOfClass:[PHAsset class]]) {
            PHAsset *phAsset = (PHAsset *)asset;
            fileName = [phAsset valueForKey:@"filename"];
        }
        // NSLog(@"图片名字:%@",fileName);
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
