//
//  NoticeMessageViewController.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/22.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "NoticeMessageViewController.h"

@interface NoticeMessageViewController ()

@property(nonatomic,strong) UIView  * headerView;
@property(nonatomic,strong) UITextView * textView;
@property(nonatomic,strong) UIButton  * btnConfirm;
@property(nonatomic,strong) UIButton  * btnCancel;


@end

@implementation NoticeMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title =  @"店铺公告";
    
    [self setUpSubViews];
    
    self.textView.text = self.notice;

}

#pragma mark  - 
-(void)setUpSubViews{

    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 300.f);
    [self.view addSubview:self.headerView];
    [self.headerView addSubview:self.textView];
    [self.headerView addSubview:self.btnConfirm];
    [self.headerView addSubview:self.btnCancel];
    
    
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerView).mas_offset(20);
        make.right.equalTo(self.headerView).mas_offset(-15);
        make.left.equalTo(self.headerView).mas_offset(15);
        make.height.mas_equalTo(150);

    }];
    
    [self.btnCancel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textView.mas_bottom).mas_offset(20);
        make.right.equalTo(self.textView);
        make.size.mas_equalTo(CGSizeMake(100, 40));
    }];
    
    
    [self.btnConfirm mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.btnCancel);
        make.right.equalTo(self.btnCancel.mas_left).mas_offset(-15);
        make.size.equalTo(self.btnCancel);
    }];
    
    
    
    
}


#pragma mark - 修改
-(void)confirmTouch:(UIButton *)sender{
    [self showHUD];
    NSDictionary* arg = @{
                          @"ince": @"set_shop_notice",
                          @"sid":[UserHandle shareInstance].userId,
                          @"notice":self.textView.text
                          };
    
    [BSDNetTool PostURL:kHost  parameters:arg succeed:^(NSDictionary *data) {
        NSLog(@"data = %@",data);
        NSInteger flag = [[data objectForKey:@"ret"]integerValue];
        if(flag == 200){
            
            [self hidHUD: @"提交成功!"];
            [self.navigationController popToRootViewControllerAnimated:YES];
            
        }else{
            [self hidHUD:[data objectForKey:@"msg"]];
        }
        
    } failure:^(NSError *error) {
        [self hidHUD:@"网络异常"];
    }];

}
#pragma mark 取消
-(void)cancelTouch:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 懒加载
-(UIView *)headerView{
    if(!_headerView){
        _headerView = [[UIView alloc]init];
    }
    return _headerView;
}

-(UITextView *)textView{
    if(!_textView){
        _textView = [[UITextView alloc]init];
        _textView.layer.masksToBounds = YES;
        _textView.layer.cornerRadius = 5.f;
        _textView.backgroundColor = [UIColor whiteColor];
        _textView.layer.borderWidth = 1.f;
        _textView.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
    }
    return _textView;
}

-(UIButton *)btnConfirm{
    if(!_btnConfirm){
        _btnConfirm = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnConfirm setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnConfirm setTitle: @"确认修改" forState:UIControlStateNormal];
        [_btnConfirm addTarget:self action:@selector(confirmTouch:) forControlEvents:UIControlEventTouchUpInside];
        _btnConfirm.layer.masksToBounds = YES;
        _btnConfirm.layer.cornerRadius = 5.f;
        _btnConfirm.backgroundColor = getColor(mainColor);
    }
    return _btnConfirm;
}

-(UIButton *)btnCancel{
    if(!_btnCancel){
        _btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnCancel setTitle: @"取消" forState:UIControlStateNormal];
        [_btnCancel addTarget:self action:@selector(cancelTouch:) forControlEvents:UIControlEventTouchUpInside];
        _btnCancel.layer.masksToBounds = YES;
        _btnCancel.layer.cornerRadius = 5.f;
        _btnCancel.backgroundColor = [UIColor lightGrayColor];

    }
    return _btnCancel;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
