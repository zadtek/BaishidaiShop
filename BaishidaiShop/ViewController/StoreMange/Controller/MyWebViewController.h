//
//  MyWebViewController.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/23.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyWebViewController : BaseViewController

@property (nonatomic, strong) NSString *webUrlStr;
@property (nonatomic, strong) NSString *titleName;


@end

NS_ASSUME_NONNULL_END
