//
//  CommentListViewController.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/22.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "CommentListViewController.h"
#import "CommentListTableViewCell.h"



@interface CommentListViewController ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

@property(nonatomic,strong) UIView* headerView;
@property(nonatomic,strong) UIView* lineView;
@property(nonatomic,strong) UITableView* tableView;


@property(nonatomic,strong) UIButton* btnAll;
@property(nonatomic,strong) UIButton* btnBest;
@property(nonatomic,strong) UIButton* btnGood;
@property(nonatomic,strong) UIButton* btnBad;


@property(nonatomic,strong) NSArray* arrayBtn;
@property(nonatomic,strong) NSString* cellIdentifier;
/**
 *  type(评论类型，0：全部，1：差评，2：中评；3：好评)
 */
@property(nonatomic,strong) NSString* typeID;

@property(nonatomic,strong) Pager* page;

@property(nonatomic,strong) CommentModel * commentModel;

@end

@implementation CommentListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.navigationItem.title =  @"用户评价";
    self.cellIdentifier = @"CommentListCell";
    self.typeID = @"0";
    
    [self creatSubViews];


    [self refreshDataSource];
    
    
}

#pragma mark - creatSubViews
-(void)creatSubViews{
    
    
    [self.view addSubview:self.headerView];
    [self.view addSubview:self.lineView];
    [self.view addSubview:self.tableView];
    
    [self.headerView addSubview:self.btnAll];
    [self.headerView addSubview:self.btnBest];
    [self.headerView addSubview:self.btnGood];
    [self.headerView addSubview:self.btnBad];
    
    self.arrayBtn = @[self.btnAll,self.btnBad,self.btnGood,self.btnBest];
    
    
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.mas_equalTo(50);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(0.8);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    
    
    CGFloat width = (SCREEN_WIDTH-50)/4;

    [self.btnAll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerView).mas_offset(10);
        make.left.equalTo(self.headerView).mas_offset(10);
        make.bottom.equalTo(self.headerView).mas_offset(-10);
        make.width.mas_equalTo(width);
    }];
    
    [self.btnBest mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.width.equalTo(self.btnAll);
        make.left.equalTo(self.btnAll.mas_right).mas_offset(10);
    }];
    
    [self.btnGood mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.width.equalTo(self.btnBest);
        make.left.equalTo(self.btnBest.mas_right).mas_offset(10);
    }];
    
    [self.btnBad mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.width.equalTo(self.btnGood);
        make.left.equalTo(self.btnGood.mas_right).mas_offset(10);
    }];
    
    
}



#pragma mark - Data Source
-(void)getData{
    
    NSDictionary* arg = @{
                          @"ince":@"get_shop_comment",
                          @"sid":[UserHandle shareInstance].userId,
                          @"type":self.typeID,
                          @"page":[WMHelper integerConvertToString:self.page.pageIndex]
                          };
    
    [BSDNetTool PostURL:kHost  parameters:arg succeed:^(NSDictionary *data) {
        NSLog(@"评价 = %@",data);
        
        if(self.page.pageIndex == 1){
            [self.page.arrayData removeAllObjects];
        }
        NSInteger flag = [[data objectForKey:@"ret"]integerValue];
        if(flag == 200){
            self.commentModel = [CommentModel mj_objectWithKeyValues:data];
            
            self.page.recordCount = [[data objectForKey:@"count"] integerValue];
            self.page.pageSize = [[data objectForKey:@"number"]integerValue];
            NSInteger empty = self.page.recordCount % self.page.pageSize;
            if(empty == 0){
                self.page.pageCount = self.page.recordCount/self.page.pageSize;
            }else{
                self.page.pageCount = self.page.recordCount/self.page.pageSize+1;
            }
            
            NSMutableArray *tempArr = [CommentModel mj_objectArrayWithKeyValuesArray:[data objectForKey:@"list"]];
            [ self.page.arrayData addObjectsFromArray:tempArr];
            
            [self loadData:self.commentModel];

        }else{
            [self alertHUD:[data objectForKey:@"msg"]];
        }
        
        [self.tableView reloadData];
        if(self.page.pageCount<=self.page.pageIndex){
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }else{
            [self.tableView.mj_footer endRefreshing];
        }
        if(self.page.pageIndex==1){
            [self.tableView.mj_header endRefreshing];
        }
        
    } failure:^(NSError *error) {
        [self alertHUD:@"网络异常"];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
    
}
-(void)refreshDataSource{
    __weak typeof(self) weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.page.pageIndex = 1;
        [weakSelf getData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoStateFooter footerWithRefreshingBlock:^{
        weakSelf.page.pageIndex ++;
        [weakSelf getData];
    }];
    [self.tableView.mj_header beginRefreshing];
}

#pragma mark - UITableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.page.arrayData.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CommentListTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier forIndexPath:indexPath];
    cell.commentModel = self.page.arrayData[indexPath.row];
    return cell;
}




#pragma mark - 切换
-(void)changeTypeBtnDidClick:(UIButton*)sender{
    self.typeID  = [NSString stringWithFormat:@"%ld",(long)sender.tag];
    sender.selected = YES;
    for (NSInteger i = 0; i<(NSInteger)self.arrayBtn.count; i++) {
        if(i != sender.tag){
            UIButton* btn = (UIButton*)self.arrayBtn[i];
            btn.selected = NO;
        }
    }
    [self.tableView.mj_header beginRefreshing];
}

#pragma mark - 
-(void)loadData:(CommentModel *)model{
    if(model){
        [self.btnAll setTitle:[NSString stringWithFormat:@"全部(%@)",model.totalNum] forState:UIControlStateNormal];
        [self.btnBest setTitle:[NSString stringWithFormat:@"好评(%@)",model.bestNum] forState:UIControlStateNormal];
        [self.btnGood setTitle:[NSString stringWithFormat:@"中评(%@)",model.goodNum] forState:UIControlStateNormal];
        [self.btnBad setTitle:[NSString stringWithFormat:@"差评(%@)",model.badNum] forState:UIControlStateNormal];
    }
}

#pragma mark - 懒加载

-(UITableView *)tableView{
    if(!_tableView){
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.emptyDataSetDelegate = self;
        _tableView.emptyDataSetSource = self;
        _tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
        [_tableView registerClass:[CommentListTableViewCell class] forCellReuseIdentifier:self.cellIdentifier];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        
        _tableView.estimatedRowHeight = 44;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        
    }
    return _tableView;
}
-(UIView *)headerView{
    if(!_headerView){
        _headerView = [[UIView alloc]init];
        _headerView.backgroundColor = [UIColor whiteColor];
    }
    return _headerView;
}

-(UIView *)lineView{
    
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    }
    return _lineView;

}

-(UIButton *)btnAll{
    if(!_btnAll){
        _btnAll  = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnAll.tag = 0;
        _btnAll.titleLabel.font = [UIFont systemFontOfSize:14.f];
        [_btnAll setBackgroundImage:[UIImage imageNamed:@"icon-comment-default"] forState:UIControlStateNormal];
        [_btnAll setBackgroundImage:[UIImage imageNamed:@"icon-comment-enter"] forState:UIControlStateSelected];
        [_btnAll setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [_btnAll setTitleColor:[UIColor colorWithRed:113/255.f green:113/255.f blue:113/255.f alpha:1.0] forState:UIControlStateNormal];
        _btnAll.selected = YES;
        [_btnAll addTarget:self action:@selector(changeTypeBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        [_btnAll setTitle:[NSString stringWithFormat:@"全部(%@)",@"0"] forState:UIControlStateNormal];

    }
    return _btnAll;
}

-(UIButton *)btnBest{
    if(!_btnBest){
        _btnBest = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnBest.tag = 3;
        _btnBest.titleLabel.font = [UIFont systemFontOfSize:14.f];
        [_btnBest setBackgroundImage:[UIImage imageNamed:@"icon-comment-default"] forState:UIControlStateNormal];
        [_btnBest setBackgroundImage:[UIImage imageNamed:@"icon-comment-enter"] forState:UIControlStateSelected];
        [_btnBest setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [_btnBest setTitleColor:[UIColor colorWithRed:113/255.f green:113/255.f blue:113/255.f alpha:1.0] forState:UIControlStateNormal];
        [_btnBest addTarget:self action:@selector(changeTypeBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        [_btnBest setTitle:[NSString stringWithFormat:@"好评(%@)",@"0"] forState:UIControlStateNormal];

    }
    return _btnBest;
}


-(UIButton *)btnGood{
    if(!_btnGood){
        _btnGood = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnGood.tag = 2;
        _btnGood.titleLabel.font = [UIFont systemFontOfSize:14.f];
        [_btnGood setBackgroundImage:[UIImage imageNamed:@"icon-comment-default"] forState:UIControlStateNormal];
        [_btnGood setBackgroundImage:[UIImage imageNamed:@"icon-comment-enter"] forState:UIControlStateSelected];
        [_btnGood setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [_btnGood setTitleColor:[UIColor colorWithRed:113/255.f green:113/255.f blue:113/255.f alpha:1.0] forState:UIControlStateNormal];
        [_btnGood addTarget:self action:@selector(changeTypeBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        [_btnGood setTitle:[NSString stringWithFormat:@"中评(%@)",@"0"] forState:UIControlStateNormal];

    }
    return _btnGood;
}

-(UIButton *)btnBad{
    if(!_btnBad){
        _btnBad = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnBad.tag = 1;
        _btnBad.titleLabel.font = [UIFont systemFontOfSize:14.f];
        [_btnBad setBackgroundImage:[UIImage imageNamed:@"icon-comment-default"] forState:UIControlStateNormal];
        [_btnBad setBackgroundImage:[UIImage imageNamed:@"icon-comment-enter"] forState:UIControlStateSelected];
        [_btnBad setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [_btnBad setTitleColor:[UIColor colorWithRed:113/255.f green:113/255.f blue:113/255.f alpha:1.0] forState:UIControlStateNormal];
        [_btnBad addTarget:self action:@selector(changeTypeBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        [_btnBad setTitle:[NSString stringWithFormat:@"差评(%@)",@"0"] forState:UIControlStateNormal];

    }
    return _btnBad;
}



-(Pager *)page{
    if(!_page){
        _page = [[Pager alloc]init];
        _page.pageIndex = 1;
    }
    return _page;
}


#pragma mark - DZEmptyData 协议实现
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    return [[NSAttributedString alloc] initWithString:@"暂无数据" attributes:@{NSFontAttributeName :[UIFont boldSystemFontOfSize:17.0],NSForegroundColorAttributeName:[UIColor grayColor]}];
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView
{
    return -64;
}
- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return YES;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
