//
//  StoreMangeViewController.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/18.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "StoreMangeViewController.h"
#import "StroeMessageViewController.h"
#import "ShopListViewController.h"
#import "PromotionSettingViewController.h"
#import "GoodsListManagerViewController.h"
#import "CommentListViewController.h"
#import "AccountMessageViewController.h"
#import "NoticeMessageViewController.h"
#import "MySettingViewController.h"

#import "StoreManageCollectionReusableView.h"
#import "StoreManageCollectionViewCell.h"

#import "StoreModel.h"



#define KImageMargin 10.f
#define Header_H 250.f
#define Item_H 100.f


@interface StoreMangeViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UINavigationControllerDelegate>

@property (nonatomic, strong) UICollectionView  *collectionView;


@property(nonatomic,strong) NSArray * arrayTitle;
@property(nonatomic,strong) NSArray * arrayImg;

@property(nonatomic,strong) StoreModel * storeModel;

@end

@implementation StoreMangeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"";
    self.navigationController.delegate = self;
    self.navigationController.navigationBar.hidden = YES;
    
    self.arrayTitle=  @[@"用户评价",@"财务信息",@"商品管理",@"店铺公告",@"店铺分类",@"促销设置",@"联系业务员",@"设置"];
    self.arrayImg =   @[@"icon-comment",@"icon-money",@"icon-goods",@"icon-notice",@"icon_dianpufenlei",@"icon_cuxiao",@"icon_lianxikefu",@"icon-setting"];
    
    [self creatSubViews];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getData];
}

#pragma mark - DataSource
-(void)getData{
    
    [self showHUD];
    
    NSDictionary* arg = @{@"ince":@"get_shop_info",
                          @"sid":[UserHandle shareInstance].userId
                          };
    
    [BSDNetTool PostURL:kHost  parameters:arg succeed:^(NSDictionary *data) {
        
        NSLog(@"data = %@",data);
        NSInteger flag = [[data objectForKey:@"ret"]integerValue];
        if(flag == 200){
            [self hidHUD];
            NSDictionary *dic = data[@"data"];
            self.storeModel = [StoreModel mj_objectWithKeyValues:dic];
            
            [self.collectionView reloadData];
        }else{
            [self hidHUD:[data objectForKey:@"msg"]];
        }
        
    } failure:^(NSError *error) {
        [self hidHUD:@"网络异常"];
    }];
    
}

-(void)creatSubViews{
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT- TABBAR_HEIGHT) collectionViewLayout:flowLayout];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.collectionView];
    
    [self.collectionView registerClass:[StoreManageCollectionViewCell class] forCellWithReuseIdentifier:@"CollectionView"];
    [self.collectionView registerClass:[StoreManageCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"CollectionViewHeader"];
}
#pragma mark - ******* collectionview代理方法 *******
//设置分区数（必须实现）
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

//设置每个分区的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.arrayTitle.count;
    
    
}
//设置item大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(SCREEN_WIDTH/2, Item_H);
    
}
//设置返回每个item的属性必须实现
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    StoreManageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionView" forIndexPath:indexPath];
    [cell setDataWithImage:self.arrayImg[indexPath.row] title:self.arrayTitle[indexPath.row]];
    
    CALayer* border = [[CALayer alloc]init];
    border.frame = CGRectMake(0,Item_H - 1, SCREEN_WIDTH/2, 1.f);
    border.backgroundColor = [UIColor colorWithRed:236/255.f green:237/255.f blue:239/255.f alpha:1.0].CGColor;
    [cell.layer addSublayer:border];
    if (indexPath.row%2==0) {//如果是偶数
        border = [[CALayer alloc]init];
        border.frame = CGRectMake(SCREEN_WIDTH/2-1, 0.f, 1.f, Item_H);
        border.backgroundColor = [UIColor colorWithRed:236/255.f green:237/255.f blue:239/255.f alpha:1.0].CGColor;
        [cell.layer addSublayer:border];
    }
    
    
    
    return cell;
}

//行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0.01;
}

//列间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
//分区缩进
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}


//头视图大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(SCREEN_WIDTH, Header_H);
}

//对头视图或者尾视图进行设置
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *reusableView = nil;
    
    if (kind == UICollectionElementKindSectionHeader){
        
        StoreManageCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"CollectionViewHeader" forIndexPath:indexPath];
        
        headerView.storeModel = self.storeModel;
        
        //点击头像
        headerView.LogoImageBlock = ^{
            
            StroeMessageViewController* controller = [[StroeMessageViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
            
        };
        
        headerView.btnChangeStatusBlock = ^(NSInteger tag) {
            if (tag == 10001) {
                
                [self changeStatusTouch];
                
            }else{
                
                //修改营业时间
                NSArray *time = @[@"0:00", @"0:30", @"1:00", @"1:30", @"2:00", @"2:30", @"3:00", @"3:30",@"4:00", @"4:30",@"5:00", @"5:30",@"6:00", @"6:30",@"7:00", @"7:30",@"8:00", @"8:30",@"9:00", @"9:30",@"10:00", @"10:30",@"11:00", @"11:30",@"12:00", @"12:30",@"13:00", @"13:30",@"14:00", @"14:30",@"15:00", @"15:30",@"16:00", @"16:30",@"17:00", @"17:30",@"18:00", @"18:30",@"19:00", @"19:30",@"20:00", @"20:30",@"21:00", @"21:30",@"22:00", @"22:30",@"23:00", @"23:30"];
                NSArray *dataSources = @[time, time];
                [CGXPickerView showStringPickerWithTitle:@"修改营业时间" DataSource:dataSources DefaultSelValue:@[@"0",@"0"] IsAutoSelect:NO Manager:nil ResultBlock:^(id selectValue, id selectRow) {
                    
                    NSArray *array = [NSArray arrayWithArray:selectValue];
                    NSString * open_time = [array[0] isEqualToString: @"0"]? @"0:00":array[0];
                    NSString * close_time = [array[1] isEqualToString: @"0"]? @"0:00":array[1];
                    
                    [self showHUD];
                    NSDictionary* arg = @{
                                          @"ince":@"set_shop_open_time",
                                          @"sid":[UserHandle shareInstance].userId,
                                          @"open_time": open_time,
                                          @"close_time":close_time
                                          };
                    [BSDNetTool PostURL:kHost  parameters:arg succeed:^(NSDictionary *data) {
                        NSInteger flag = [[data objectForKey:@"ret"]integerValue];
                        if(flag == 200){
                            
                            NSString *text = [NSString stringWithFormat:@"%@ ~ %@", open_time, close_time];
                            headerView.labelStoreTime.text = [NSString stringWithFormat:@"营业时间：%@",text];
                            [self hidHUD];
                        }else{
                            
                            [self hidHUD:[data objectForKey:@"msg"]];
                        }
                    } failure:^(NSError *error) {
                        [self hidHUD:@"网络异常"];
                    }];
                    
                }];
                
                
            }
        };
        
        reusableView = headerView;
        
    }
    
    return reusableView;
    
}

//点击item
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger index = indexPath.row;
    switch (index) {
        case 0:
        {
            CommentListViewController* controller = [[CommentListViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
            
        }
            break;
        case 1:
        {
            AccountMessageViewController * controller = [[AccountMessageViewController alloc]init];
            controller.storeModel = self.storeModel;
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
        case 2:
        {
            GoodsListManagerViewController* controller =[[GoodsListManagerViewController alloc]init];
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
        case 3:
        {
            NoticeMessageViewController * controller = [[NoticeMessageViewController alloc]init];
            controller.notice = self.storeModel.notice;
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
            
        case 4:
        {
            ShopListViewController* controller = [[ShopListViewController alloc] init];
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
            
        case 5:
        {
            PromotionSettingViewController* controller = [[PromotionSettingViewController alloc] init];
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
            
            
        case 6:
        {
            NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"tel:%@",self.storeModel.tel];
            UIWebView *callWebview = [[UIWebView alloc] init];
            [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]]];
            [self.view addSubview:callWebview];
        }
            break;
            
            
        case 7:
        {
            MySettingViewController  * controller = [[MySettingViewController alloc] init];
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
            
            
        default:
            break;
    }
    
    
}


#pragma mark - 修改店铺状态
-(void)changeStatusTouch{
    
    [self showHUD];
    NSDictionary* arg = @{@"ince":@"set_shop_open",
                          @"sid":[UserHandle shareInstance].userId,
                          @"close":[self.storeModel.status isEqualToString:@"1"]?@"0":@"1"
                          };
    [BSDNetTool PostURL:kHost  parameters:arg succeed:^(NSDictionary *data) {
        NSInteger flag = [[data objectForKey:@"ret"]integerValue];
        if(flag == 200){
            self.storeModel.status = [self.storeModel.status isEqualToString:@"1"]?@"0":@"1";
            [self.collectionView reloadData];
            [self hidHUD];
        }else{
            
            [self hidHUD:[data objectForKey:@"msg"]];
        }
    } failure:^(NSError *error) {
        [self hidHUD:@"网络异常"];
    }];
    
}

#pragma mark - UINavigationControllerDelegate
// 将要显示控制器
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    // 判断要显示的控制器是否是自己
    BOOL isShowPersonPage = [viewController isKindOfClass:[self class]];
    [self.navigationController setNavigationBarHidden:isShowPersonPage animated:animated];
    
}
//设置字体颜色
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;//白色
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
