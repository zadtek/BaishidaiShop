//
//  CashApplicationViewController.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/23.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "CashApplicationViewController.h"

@interface CashApplicationViewController ()

@property(nonatomic,strong) UILabel* titleLabel;
@property(nonatomic,strong) UITextField* cardTF;
@property(nonatomic,strong) UITextField* moneyTF;
@property(nonatomic,strong) UIButton* confirmBtn;
@property(nonatomic,strong) UILabel* markLabel;


@end

@implementation CashApplicationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"申请提现";
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    
    [self setUpSubViews];
    
    
    self.moneyTF.text = self.storeModel.balance;
    self.titleLabel.text = [NSString stringWithFormat:@"%@",self.storeModel.top_note];
    
    NSString* bottom_note = self.storeModel.bottom_note;
    NSMutableAttributedString* attributeStr =[[NSMutableAttributedString alloc]initWithString:bottom_note];
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];
    [attributeStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [bottom_note length])];
    _markLabel.attributedText =attributeStr;
    
}

-(void)setUpSubViews{
    
    [self.view addSubview:self.titleLabel];
    [self.view addSubview:self.cardTF];
    [self.view addSubview:self.moneyTF];
    
    [self.view addSubview:self.confirmBtn];
    [self.view addSubview:self.markLabel];
    
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.left.equalTo(self.view).offset(15);
        make.right.equalTo(self.view).offset(-15);
        make.height.mas_equalTo(50);
    }];
    
    
    [self.cardTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom);
        make.left.right.equalTo(self.view);
        make.height.equalTo(self.titleLabel);
    }];
    
    [self.moneyTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.cardTF.mas_bottom);
        make.left.right.equalTo(self.view);
        make.height.equalTo(self.cardTF);
    }];
    
    
    [self.confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.moneyTF.mas_bottom).offset(50);
        make.left.right.equalTo(self.titleLabel);
        make.height.equalTo(self.titleLabel);
    }];
    
    [self.markLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.confirmBtn.mas_bottom);
        make.left.right.equalTo(self.titleLabel);
        make.height.equalTo(self.titleLabel);
    }];
    
    
}

#pragma mark -
-(void)confirmBtnTouch:(UIButton *)sender{
    
    if (_moneyTF.text.length == 0) {
        ShowMessage(@"请输入提现金额");
        return ;
    }
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"确认提现吗？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
        [self showHUD];
        NSDictionary* arg = @{
                              @"ince":@"set_shop_deposit",
                              @"sid":[UserHandle shareInstance].userId,
                              @"uid":self.storeModel.uid,
                              @"amount":self.moneyTF.text
                              };

        [BSDNetTool PostURL:kHost  parameters:arg succeed:^(NSDictionary *data) {
            NSLog(@"data = %@",data);
            NSInteger flag = [[data objectForKey:@"ret"]integerValue];
            if(flag == 200){
                [self hidHUD];
                [self.navigationController popToRootViewControllerAnimated:YES];
                
            }else{
                [self hidHUD:[data objectForKey:@"msg"]];
            }
            
        } failure:^(NSError *error) {
            [self hidHUD:@"网络异常"];
        }];
        
        
    }];
    UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alertController addAction:defaultAction];
    [alertController addAction:cancleAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}

#pragma mark - 懒加载

-(UILabel *)titleLabel{
    if(!_titleLabel){
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.font = [UIFont systemFontOfSize:12.f];
        _titleLabel.textColor = [UIColor colorWithRed:64/255.f green:64/255.f blue:64/255.f alpha:1.0];
    }
    return _titleLabel;
}

-(UITextField *)cardTF{
    if(!_cardTF){
        _cardTF = [[UITextField alloc]init];
        _cardTF.backgroundColor = [UIColor whiteColor];
        _cardTF.borderStyle=UITextBorderStyleNone;
        _cardTF.layer.borderColor =[UIColor colorWithRed:231/255.f green:231/255.f blue:231/255.f alpha:1.0].CGColor;
        _cardTF.layer.borderWidth = 1.0f;
        _cardTF.layer.masksToBounds = YES;
        _cardTF.userInteractionEnabled = NO;
        _cardTF.font = [UIFont systemFontOfSize:14.f];
        _cardTF.textColor = [UIColor colorWithRed:64/255.f green:64/255.f blue:64/255.f alpha:1.0];
        _cardTF.text = [NSString stringWithFormat:@"%@ : %@",self.storeModel.owner_name,self.storeModel.owner_bank];
        UILabel* label = [[UILabel alloc]init];
        label.frame = CGRectMake(0, 0, 15, 50);
        label.font = [UIFont systemFontOfSize:14.f];
        label.textColor = [UIColor colorWithRed:64/255.f green:64/255.f blue:64/255.f alpha:1.0];
        label.textAlignment = NSTextAlignmentCenter;
        _cardTF.leftView = label;
        _cardTF.leftViewMode =UITextFieldViewModeAlways;
    }
    return _cardTF;
}

-(UITextField *)moneyTF{
    if(!_moneyTF){
        _moneyTF = [[UITextField alloc]init];
        _moneyTF.backgroundColor = [UIColor whiteColor];
        _moneyTF.borderStyle=UITextBorderStyleNone;
        _moneyTF.font = [UIFont systemFontOfSize:14.f];
        _moneyTF.keyboardType = UIKeyboardTypeNumberPad;
        //        _moneyTF.userInteractionEnabled = NO;
        _moneyTF.placeholder =@"请输入提现金额";
        _moneyTF.textColor = [UIColor colorWithRed:64/255.f green:64/255.f blue:64/255.f alpha:1.0];
        UIView* leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 90, 50.f)];
        UILabel* label = [[UILabel alloc]init];
        label.frame = CGRectMake(15, 0, 70, 50);
        label.font = [UIFont systemFontOfSize:14.f];
        label.textColor =[UIColor colorWithRed:64/255.f green:64/255.f blue:64/255.f alpha:1.0];
        label.text = @"提现金额";
        label.textAlignment = NSTextAlignmentLeft;
        [leftView addSubview:label];
        label = [[UILabel alloc]initWithFrame:CGRectMake(90-8, 15, 2, 20.f)];
        label.backgroundColor = getColor(mainColor);
        [leftView addSubview:label];
        _moneyTF.leftView = leftView;
        _moneyTF.leftViewMode =UITextFieldViewModeAlways;
    }
    return _moneyTF;
}


-(UIButton *)confirmBtn{
    if(!_confirmBtn){
        _confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _confirmBtn.backgroundColor = getColor(mainColor);
        [_confirmBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_confirmBtn setTitle:@"确认提现" forState:UIControlStateNormal];
        [_confirmBtn addTarget:self action:@selector(confirmBtnTouch:) forControlEvents:UIControlEventTouchUpInside];
        _confirmBtn.layer.masksToBounds = YES;
        _confirmBtn.layer.cornerRadius = 5.f;

    }
    return _confirmBtn;
}

-(UILabel *)markLabel{
    if(!_markLabel){
        _markLabel = [[UILabel alloc]init];
        _markLabel.font = [UIFont systemFontOfSize:12.f];
        _markLabel.textColor = [UIColor colorWithRed:64/255.f green:64/255.f blue:64/255.f alpha:1.0];
        _markLabel.numberOfLines = 0;
        _markLabel.lineBreakMode = NSLineBreakByWordWrapping;
        //        NSString* str = @"温馨提示:根据银行情况1-3个工作日到账";
        //        NSMutableAttributedString* attributeStr =[[NSMutableAttributedString alloc]initWithString:str];
        //        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        //        [paragraphStyle setLineSpacing:5];
        //        [attributeStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
        //        _markLabel.attributedText =attributeStr;
    }
    return _markLabel;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
