//
//  POISearchViewController.h
//  rrbi
//
//  Created by mac book on 2018/12/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^ReturnPoiData)(NSDictionary *dic);

@interface POISearchViewController : BaseViewController

@property (copy, nonatomic) ReturnPoiData returnPoiData;


@end

NS_ASSUME_NONNULL_END
