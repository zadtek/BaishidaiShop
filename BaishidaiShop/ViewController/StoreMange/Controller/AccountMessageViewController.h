//
//  AccountMessageViewController.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/22.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoreModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface AccountMessageViewController : BaseViewController

@property(nonatomic,strong) StoreModel * storeModel;

@end

NS_ASSUME_NONNULL_END
