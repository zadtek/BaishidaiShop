//
//  NoticeMessageViewController.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/22.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NoticeMessageViewController : BaseViewController

@property(nonatomic,strong) NSString  *notice;

@end

NS_ASSUME_NONNULL_END
