//
//  StoreModel.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/22.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface StoreModel : NSObject



@property(nonatomic,copy) NSString* shop_sale;
@property(nonatomic,copy) NSString* status;
@property(nonatomic,copy) NSString* open_time;
@property(nonatomic,copy) NSString* site_name;


@property(nonatomic,copy) NSString* today_order_num;
@property(nonatomic,copy) NSString* notice;
@property(nonatomic,copy) NSString* tel;
@property(nonatomic,copy) NSString* today;
@property(nonatomic,copy) NSString* balanc_desc;



@property(nonatomic,copy) NSString* top_note;
@property(nonatomic,copy) NSString* balance;
@property(nonatomic,copy) NSString* owner_name;

@property(nonatomic,copy) NSString* owner_bank;

@property(nonatomic,copy) NSString* uid;

@property(nonatomic,copy) NSString* status_remark;

@property(nonatomic,copy) NSString* phone;

@property(nonatomic,copy) NSString* close_time;

@property(nonatomic,copy) NSString* sale_sub;


@property(nonatomic,copy) NSString* mobile;

@property(nonatomic,copy) NSString* sale_full;
@property(nonatomic,copy) NSString* site_id;

@property(nonatomic,copy) NSString* freeship_amount;

@property(nonatomic,copy) NSString* bottom_note;

@property(nonatomic,copy) NSString* note;

@property(nonatomic,copy) NSString* logo;

@property(nonatomic,copy) NSString* user_sale;

@property(nonatomic,copy) NSString* address;

@property(nonatomic,copy) NSString* send;


@property(nonatomic,copy) NSString* frozen_money;
@property(nonatomic,copy) NSString* frozen_money_desc;



@property(nonatomic,copy) NSString* man;
@property(nonatomic,copy) NSString* jian;
@property(nonatomic,copy) NSString* store_id;


@property(nonatomic,copy) NSString* cate_name;
@property(nonatomic,copy) NSString* catestatus;


@property(nonatomic,copy) NSString* shop_address;
@property(nonatomic,copy) NSString* shop_name;
@property(nonatomic,copy) NSString* agent_code;
@property(nonatomic,copy) NSString* shop_tel;



@property(nonatomic,copy) NSString* shop_owner;
@property(nonatomic,copy) NSString* bank_name;
@property(nonatomic,copy) NSString* bank_host;
@property(nonatomic,copy) NSString* bank_num;


@property(nonatomic,copy) NSString* longitude;
@property(nonatomic,copy) NSString* latitude;


@property(nonatomic,copy) NSString* license;
@property(nonatomic,copy) NSString* other_cert;
@property(nonatomic,copy) NSString* shop_back;
@property(nonatomic,copy) NSString* shop_logo;


@end

NS_ASSUME_NONNULL_END
