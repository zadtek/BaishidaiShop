//
//  AccountModel.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/23.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AccountModel : NSObject

@property(nonatomic,copy) NSString* addtime;
@property(nonatomic,copy) NSString* amount;
@property(nonatomic,copy) NSString* givetime;
@property(nonatomic,copy) NSString* h5_url;
@property(nonatomic,copy) NSString* mark;
@property(nonatomic,copy) NSString* item_id;
@property(nonatomic,copy) NSString* order_id;
@property(nonatomic,copy) NSString* page_title;

@property(nonatomic,copy) NSString* paytime;
@property(nonatomic,copy) NSString* site_id;
@property(nonatomic,copy) NSString* status;

@end

NS_ASSUME_NONNULL_END
