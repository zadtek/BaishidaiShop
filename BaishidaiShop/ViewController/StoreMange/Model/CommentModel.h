//
//  CommentModel.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/22.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommentModel : NSObject


@property(nonatomic,copy) NSString* totalNum;
@property(nonatomic,copy) NSString* bestNum;
@property(nonatomic,copy) NSString* goodNum;
@property(nonatomic,copy) NSString* badNum;


@property(nonatomic,copy) NSString* sid;
@property(nonatomic,copy) NSString* send_done_time;
@property(nonatomic,copy) NSString* score2;
@property(nonatomic,copy) NSString* score1;
@property(nonatomic,copy) NSString* score;

@property(nonatomic,copy) NSString* righttime;
@property(nonatomic,copy) NSString* reply;
@property(nonatomic,copy) NSString* order_id;


@property(nonatomic,copy) NSString* name;
@property(nonatomic,copy) NSString* itemid;
@property(nonatomic,copy) NSString* img_url;
@property(nonatomic,copy) NSString* comment_id;
@property(nonatomic,copy) NSString* comment;
@property(nonatomic,copy) NSString* addtime;


//count = 0;
//list = "";
//msg = "\U6682\U65e0\U6570\U636e";
//num = 0;
//number = 10;
//ret = 0;
//score = 0;
//score1 = 0;
//score2 = 0;




@end

NS_ASSUME_NONNULL_END
