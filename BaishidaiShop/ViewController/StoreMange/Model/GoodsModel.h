//
//  GoodsModel.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/21.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GoodsModel : NSObject



@property(nonatomic,copy) NSString* cat_id;
@property(nonatomic,copy) NSString* catestatus;
@property(nonatomic,copy) NSString* site_id;
@property(nonatomic,copy) NSString* cat_name;


@property(nonatomic,copy) NSString* cate_name;

@property(nonatomic,copy) NSString* check_price;
@property(nonatomic,copy) NSString* default_image;
@property(nonatomic,copy) NSString* fid;

@property(nonatomic,copy) NSString* market_price;
@property(nonatomic,copy) NSString* name;
@property(nonatomic,copy) NSString* price;

@property(nonatomic,copy) NSString* shopcate;
@property(nonatomic,copy) NSString* sid;
@property(nonatomic,copy) NSString* status;

@property(nonatomic,copy) NSString* user_cat;
@property(nonatomic,copy) NSString* stock;


@end

NS_ASSUME_NONNULL_END
