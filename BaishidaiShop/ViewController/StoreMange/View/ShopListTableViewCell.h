//
//  ShopListTableViewCell.h
//  rrbi
//
//  Created by mac book on 2018/12/4.
//

#import <UIKit/UIKit.h>
#import "StoreModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface ShopListTableViewCell : UITableViewCell

@property(nonatomic,strong) StoreModel* storeModel;

@property (copy, nonatomic) void(^deleteBtnBlock)(void);

@end

NS_ASSUME_NONNULL_END
