//
//  AccountDetailTableViewCell.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/23.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "AccountDetailTableViewCell.h"


@interface AccountDetailTableViewCell ()

@property(nonatomic,strong) UILabel * dateLabel;
@property(nonatomic,strong) UILabel * cashLabel;
@property(nonatomic,strong) UILabel * statusLabel;

@end


@implementation AccountDetailTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self setUpSubViews];
    }
    return self;
}


#pragma mark - setUpSubViews
-(void)setUpSubViews{
    [self.contentView addSubview:self.dateLabel];
    [self.contentView addSubview:self.cashLabel];
    [self.contentView addSubview:self.statusLabel];
    
    float width = (SCREEN_WIDTH- 30)/3;
    
    
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
        make.width.mas_equalTo(width);
    }];
    [self.cashLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.contentView);
        make.left.equalTo(self.dateLabel.mas_right);
        make.width.mas_equalTo(width);
    }];
    
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.contentView);
        make.left.equalTo(self.cashLabel.mas_right);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    
    
}


#pragma mark - set方法
-(void)setAccountModel:(AccountModel *)accountModel{
    
    _accountModel = accountModel;
    
    self.dateLabel.text =accountModel.addtime;
    self.cashLabel.text = [NSString stringWithFormat:@"%.2f",[accountModel.amount floatValue]];
    self.statusLabel.text = accountModel.mark;
    
}


#pragma mark - 懒记载
-(UILabel *)dateLabel{
    if(!_dateLabel){
        _dateLabel = [[UILabel alloc]init];
        _dateLabel.textColor = [UIColor colorWithRed:130/255.f green:130/255.f blue:130/255.f alpha:1.0];
        _dateLabel.font = [UIFont systemFontOfSize:14.f];
    }
    return _dateLabel;
}

-(UILabel *)cashLabel{
    if(!_cashLabel){
        _cashLabel = [[UILabel alloc]init];
        _cashLabel.textColor = [UIColor redColor];
        _cashLabel.font = [UIFont systemFontOfSize:14.f];
        _cashLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _cashLabel;
}

-(UILabel *)statusLabel{
    if(!_statusLabel){
        _statusLabel = [[UILabel alloc]init];
        _statusLabel.textColor = [UIColor colorWithRed:130/255.f green:130/255.f blue:130/255.f alpha:1.0];
        _statusLabel.font = [UIFont systemFontOfSize:14.f];
        _statusLabel.textAlignment = NSTextAlignmentRight;
    }
    return _statusLabel;
}




- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
