//
//  GoodsListManagerTableViewCell.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/22.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "GoodsListManagerTableViewCell.h"
#import "TopLeftLabe.h"

@interface GoodsListManagerTableViewCell ()

@property(nonatomic,strong) UIImageView* photoImageView;
@property(nonatomic,strong) UIView* middleView;
@property(nonatomic,strong) TopLeftLabe* titleLabel;
@property(nonatomic,strong) UILabel* priceLabel;
@property(nonatomic,strong) UILabel* typeLabel;

@property(nonatomic,strong) UIView* rightView;
@property(nonatomic,strong) UIButton* priceBtn;
@property(nonatomic,strong) UIButton* statusBtn;
@property(nonatomic,strong) UIImageView* lineView;

@end

@implementation GoodsListManagerTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self setUpSubViews];
    }
    return self;
}

#pragma mark  -
-(void)setUpSubViews{
    [self.contentView addSubview:self.photoImageView];
    [self.contentView addSubview:self.middleView];
    [self.middleView addSubview:self.titleLabel];
    [self.middleView addSubview:self.priceLabel];
    [self.middleView addSubview:self.typeLabel];
    
    [self.contentView addSubview:self.rightView];
    [self.rightView addSubview:self.priceBtn];
    [self.rightView addSubview:self.statusBtn];
    [self.contentView addSubview:self.lineView];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView);
        make.height.mas_equalTo(1);
        make.left.right.equalTo(self.contentView);
    }];
    
    [self.photoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.contentView).offset(10);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    
    
    
    
    [self.rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.photoImageView);
        make.right.equalTo(self.contentView).offset(-10);
        make.width.mas_equalTo(60);
    }];
    
    
    [self.middleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.rightView);
        make.right.equalTo(self.rightView.mas_left).offset(-5);
        make.left.equalTo(self.photoImageView.mas_right).offset(5);
    }];
    
    
    [self.priceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.rightView);
        make.width.equalTo(self.rightView);
        make.height.mas_equalTo(20);
    }];
    
    [self.statusBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(self.rightView);
        make.width.equalTo(self.rightView);
        make.height.equalTo(self.priceBtn);
    }];
    
    
    
    [self.typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(self.middleView);
        make.width.equalTo(self.middleView);
        make.height.mas_equalTo(15);
    }];
    
    
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(self.typeLabel.mas_top);
        make.width.equalTo(self.middleView);
        make.height.equalTo(self.typeLabel);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.middleView);
        make.bottom.equalTo(self.priceLabel.mas_top);
        make.width.equalTo(self.middleView);
    }];
    

}


#pragma mark - 修改价格
-(void)updatePriceButtonClick:(UIButton *)sender{
    if(self.delegate && [self.delegate respondsToSelector:@selector(updateGoodsPrice:)]){
        [self.delegate updateGoodsPrice:self.goodsModel];
    }
}
#pragma mark - 修改状态
-(void)pushlishButtonClicked:(UIButton *)sender{
    if(self.delegate && [self.delegate respondsToSelector:@selector(publishGoods:)]){
        [self.delegate publishGoods:self.goodsModel];
    }
}
#pragma mark - 修改标题
-(void) labelTouchUpInside:(UITapGestureRecognizer *)recognizer{

    if(self.delegate && [self.delegate respondsToSelector:@selector(updateGoodsTitle:)]){
        [self.delegate updateGoodsTitle:self.goodsModel];
    }
}
#pragma mark - 修改图片
-(void) photoImageViewTouchUpInside:(UITapGestureRecognizer *)recognizer{

    if(self.delegate && [self.delegate respondsToSelector:@selector(updateGoodsImage:)]){
        [self.delegate updateGoodsImage:self.goodsModel];
    }
}


-(void)setGoodsModel:(GoodsModel *)goodsModel{
    
    _goodsModel = goodsModel;
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:goodsModel.default_image] placeholderImage:[UIImage imageNamed: @"placeholder"]];


    self.titleLabel.text = goodsModel.name;
    self.priceLabel.text = [NSString stringWithFormat: @"结算价：%@   销售价：%@",goodsModel.check_price,goodsModel.price];
    
    
    if([goodsModel.status integerValue] == 0){
        _statusBtn.backgroundColor = [UIColor lightGrayColor];
        [self.statusBtn setTitle: @"已下架" forState:UIControlStateNormal];
    }else{
        _statusBtn.backgroundColor = getColor(mainColor);
        [self.statusBtn setTitle: @"已上架" forState:UIControlStateNormal];
        
    }
    
    self.typeLabel.text = [NSString stringWithFormat: @"分类：%@",goodsModel.cate_name];
}

-(UIImageView *)photoImageView{
    if(!_photoImageView){
        _photoImageView = [[UIImageView alloc]init];
        _photoImageView.userInteractionEnabled=YES;
        UITapGestureRecognizer *labelTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(photoImageViewTouchUpInside:)];
        [_photoImageView addGestureRecognizer:labelTapGestureRecognizer];
    }
    return _photoImageView;
}
-(UIView *)middleView{
    if(!_middleView){
        _middleView = [[UIView alloc]init];
    }
    return _middleView;
}

-(TopLeftLabe *)titleLabel{
    if(!_titleLabel){
        _titleLabel = [[TopLeftLabe alloc]init];
        _titleLabel.numberOfLines = 0;
        _titleLabel.textColor = [UIColor colorWithRed:60/255.f green:60/255.f blue:60/255.f alpha:1.0];
        _titleLabel.font = kFontNameSize(14);
        [_titleLabel setContentMode:UIViewContentModeTop];
        _titleLabel.userInteractionEnabled=YES;
        UITapGestureRecognizer *labelTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(labelTouchUpInside:)];
        [_titleLabel addGestureRecognizer:labelTapGestureRecognizer];
        
    }
    return _titleLabel;
}
-(UILabel *)typeLabel{
    if(!_typeLabel){
        _typeLabel = [[UILabel alloc]init];
        _typeLabel.textColor = [UIColor colorWithRed:125/255.f green:125/255.f blue:125/255.f alpha:1.f];
        _typeLabel.font = kFontNameSize(12);
    }
    return _typeLabel;
}
-(UILabel *)priceLabel{
    if(!_priceLabel){
        _priceLabel = [[UILabel alloc]init];
        _priceLabel.textColor = [UIColor colorWithRed:125/255.f green:125/255.f blue:125/255.f alpha:1.f];
        _priceLabel.font = kFontNameSize(12);
        
    }
    return _priceLabel;
}

-(UIView *)rightView{
    if(!_rightView){
        _rightView = [[UIView alloc]init];
    }
    return _rightView;
}

-(UIButton *)priceBtn{
    if(!_priceBtn){
        _priceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_priceBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_priceBtn setTitle: @"修改价格" forState:UIControlStateNormal];
        _priceBtn.backgroundColor = getColor(mainColor);
        _priceBtn.titleLabel.font = kFontNameSize(14);
        [_priceBtn addTarget:self action:@selector(updatePriceButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        _priceBtn.layer.masksToBounds = YES;
        _priceBtn.layer.cornerRadius = 3;
    }
    return _priceBtn;
}

-(UIButton *)statusBtn{
    if(!_statusBtn){
        _statusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_statusBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_statusBtn setTitle: @"已上架" forState:UIControlStateNormal];
        _statusBtn.backgroundColor = getColor(mainColor);
        _statusBtn.titleLabel.font = kFontNameSize(14);
        [_statusBtn addTarget:self action:@selector(pushlishButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        _statusBtn.layer.masksToBounds = YES;
        _statusBtn.layer.cornerRadius = 3;
    }
    return _statusBtn;
}

-(UIImageView *)lineView{
    if(!_lineView){
        _lineView = [[UIImageView alloc]init];
        _lineView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    }
    return _lineView;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
