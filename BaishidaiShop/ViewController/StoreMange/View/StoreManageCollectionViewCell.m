//
//  StoreManageCollectionViewCell.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/22.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "StoreManageCollectionViewCell.h"

@interface StoreManageCollectionViewCell ()

@property(nonatomic,strong) UIImageView* photoImageView;
@property(nonatomic,strong) UILabel* titleLabel;


@end


@implementation StoreManageCollectionViewCell


- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        [self setUpView];
    }
    return self;
}

- (void)setUpView{
    
    
    [self setBackgroundColor:[UIColor whiteColor]];
    
    self.photoImageView = [[UIImageView alloc]init];
    [self addSubview:self.photoImageView];
    
    
    
    self.titleLabel = [[ UILabel alloc]init];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.font = kFontNameSize(14);
    [self addSubview:self.titleLabel];
    
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self);
        make.height.mas_equalTo(30);
        make.width.equalTo(self);
        
    }];
    
    
    [self.photoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.bottom.equalTo(self.titleLabel.mas_top);
        make.size.mas_equalTo(CGSizeMake(60, 60));

    }];
    
    
  
    
}


-(void)setDataWithImage:(NSString *)image title:(NSString *)title{
    
    [self.photoImageView setImage:[UIImage imageNamed:image]];
    self.titleLabel.text = title;
}




@end
