//
//  CommentListTableViewCell.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/22.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommentListTableViewCell : UITableViewCell

@property(nonatomic,strong) CommentModel * commentModel;

@end

NS_ASSUME_NONNULL_END
