//
//  StoreManageCollectionReusableView.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/22.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoreModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface StoreManageCollectionReusableView : UICollectionReusableView

@property(nonatomic,strong) UILabel* labelStoreTime;

@property(nonatomic,strong) StoreModel* storeModel;

@property (copy, nonatomic) void(^btnChangeStatusBlock)(NSInteger tag);

@property (copy, nonatomic) void(^LogoImageBlock)(void);



@end

NS_ASSUME_NONNULL_END
