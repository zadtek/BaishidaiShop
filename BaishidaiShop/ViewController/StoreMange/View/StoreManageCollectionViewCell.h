//
//  StoreManageCollectionViewCell.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/22.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StoreManageCollectionViewCell : UICollectionViewCell

-(void)setDataWithImage:(NSString *)image title:(NSString *)title;

@end

NS_ASSUME_NONNULL_END
