//
//  AccountDetailTableViewCell.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/23.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AccountModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AccountDetailTableViewCell : UITableViewCell


@property(nonatomic,strong) AccountModel * accountModel;


@end

NS_ASSUME_NONNULL_END
