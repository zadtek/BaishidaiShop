//
//  CommentListTableViewCell.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/22.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "CommentListTableViewCell.h"


@interface CommentListTableViewCell ()

@property(nonatomic,strong) UIImageView* avatarImageView;
@property(nonatomic,strong) UILabel* userNameLabel;
@property(nonatomic,strong) UILabel* createDateLabel;
@property(nonatomic,strong) UILabel* commentLabel;
@property(nonatomic,strong) UIView* starView;

@property(nonatomic,strong) NSMutableArray* starArray;

@end


@implementation CommentListTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self setUpSubViews];
    }
    return self;
}

#pragma mark - setUpSubViews
-(void)setUpSubViews{
    
    
    [self.contentView addSubview:self.avatarImageView];
    [self.contentView addSubview:self.userNameLabel];
    [self.contentView addSubview:self.createDateLabel];
    [self.contentView addSubview:self.starView];
    [self.contentView addSubview:self.commentLabel];
    
    
    [self.avatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.contentView).offset(10);
        make.size.mas_equalTo(CGSizeMake(35, 35));
    }];
    
    
    [self.userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.avatarImageView);
        make.height.mas_equalTo(20);
        make.left.equalTo(self.avatarImageView.mas_right).offset(10);
        make.width.mas_equalTo(100);

    }];
    [self.createDateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.equalTo(self.userNameLabel);
        make.left.equalTo(self.userNameLabel.mas_right);
        make.right.equalTo(self.contentView).offset(-10);
        
    }];
    
    
    [self.starView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userNameLabel.mas_bottom).offset(5);
        make.left.equalTo(self.userNameLabel);
        make.right.equalTo(self.createDateLabel);
        make.height.mas_equalTo(15);

    }];
    
    
    
    self.starArray =[[NSMutableArray alloc]init];
    for (int i=0; i<5; i++) {
        UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(20*i, 0, 15, 15);
        btn.tag = i;
        [btn setBackgroundImage:[UIImage imageNamed:@"icon-star-default"] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageNamed:@"icon-star-enter"] forState:UIControlStateSelected];
        [self.starView addSubview:btn];
        [self.starArray addObject:btn];
    }
    
    [self.commentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.starView.mas_bottom).offset(5);
        make.left.equalTo(self.avatarImageView);
        make.right.equalTo(self.createDateLabel);
        make.bottom.equalTo(self.contentView).offset(-10);
        
    }];
    
    
}


#pragma mark -

-(void)setCommentModel:(CommentModel *)commentModel{
    
    _commentModel = commentModel;
    
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:commentModel.img_url] placeholderImage:[UIImage imageNamed: @"placeholder"]];
    self.userNameLabel.text = commentModel.name;
    self.createDateLabel.text = commentModel.addtime;
    self.commentLabel.text = commentModel.comment;
    
    NSInteger star = [commentModel.score integerValue];
    for (UIButton *btn in self.starArray) {
        if(btn.tag< star){
            btn.selected = YES;
        }else{
            btn.selected = NO;
        }
    }
}

#pragma mark - 懒加载
-(UIImageView *)avatarImageView{
    if(!_avatarImageView){
        _avatarImageView = [[UIImageView alloc]init];
        _avatarImageView.layer.masksToBounds = YES;
        _avatarImageView.layer.cornerRadius = 35/2.f;
    }
    return _avatarImageView;
}

-(UILabel *)userNameLabel{
    if(!_userNameLabel){
        _userNameLabel =[[UILabel alloc]init];
        _userNameLabel.textColor = [UIColor colorWithRed:142/255.f green:142/255.f blue:142/255.f alpha:1.0];
        _userNameLabel.font = kFontNameSize(14);
        _userNameLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    }
    return _userNameLabel;
}

-(UILabel *)createDateLabel{
    if(!_createDateLabel){
        _createDateLabel =[[UILabel alloc]init];
        _createDateLabel.textColor = [UIColor colorWithRed:142/255.f green:142/255.f blue:142/255.f alpha:1.0];
        _createDateLabel.font = kFontNameSize(12);
        _createDateLabel.textAlignment = NSTextAlignmentRight;
    }
    return _createDateLabel;
}

-(UILabel *)commentLabel{
    if(!_commentLabel){
        _commentLabel = [[UILabel alloc]init];
        _commentLabel.numberOfLines = 0;
        _commentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _commentLabel.font = kFontNameSize(14);
    }
    return _commentLabel;
}

-(UIView *)starView{
    if(!_starView){
        _starView = [[UIView alloc]init];
    }
    return _starView;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
