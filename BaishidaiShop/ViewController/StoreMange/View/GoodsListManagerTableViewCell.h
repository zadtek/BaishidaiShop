//
//  GoodsListManagerTableViewCell.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/22.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsModel.h"


NS_ASSUME_NONNULL_BEGIN

@protocol GoodsListManagerDelegate <NSObject>

@optional

-(void)updateGoodsPrice:(GoodsModel*)goodsModel;
-(void)publishGoods:(GoodsModel*)goodsModel;
-(void)updateGoodsTitle:(GoodsModel*)goodsModel;
-(void)updateGoodsImage:(GoodsModel*)goodsModel;

@end

@interface GoodsListManagerTableViewCell : UITableViewCell

@property(nonatomic,weak) id<GoodsListManagerDelegate> delegate;

@property(nonatomic,strong) GoodsModel* goodsModel;


@end

NS_ASSUME_NONNULL_END
