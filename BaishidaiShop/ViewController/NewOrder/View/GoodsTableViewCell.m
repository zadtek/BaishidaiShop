//
//  GoodsTableViewCell.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/21.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "GoodsTableViewCell.h"

@interface GoodsTableViewCell ()

@property(nonatomic,strong)UILabel* foodNameLabel;
@property(nonatomic,strong) UILabel* quantityLabel;
@property(nonatomic,strong) UILabel* priceLabel;
@property(nonatomic,strong) UILabel* lineLabel;

@end


@implementation GoodsTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self createSubviews];
    }
    return self;
}

#pragma mark - createSubviews
-(void)createSubviews{
    
    [self.contentView addSubview:self.foodNameLabel];
    [self.contentView addSubview:self.quantityLabel];
    [self.contentView addSubview:self.priceLabel];
    [self.contentView addSubview:self.lineLabel];
    
    
    [self.foodNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.bottom.equalTo(self.contentView);
        make.left.equalTo(self.contentView).mas_offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH*3/5);

    }];
    
    [self.quantityLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.bottom.equalTo(self.contentView);
        make.left.equalTo(self.foodNameLabel.mas_right);
        make.width.mas_equalTo(SCREEN_WIDTH/10);
        
    }];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.bottom.equalTo(self.contentView);
        make.left.equalTo(self.quantityLabel.mas_right);
        make.right.equalTo(self.contentView).mas_offset(-10);
        
    }];
    
    [self.lineLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(self.contentView);
        make.left.equalTo(self.contentView).mas_offset(10);
        make.right.equalTo(self.contentView).mas_offset(-10);
        make.height.mas_equalTo(0.8);

    }];
    
    
    
}



#pragma mark -

-(void)setOrderModel:(OrderModel *)orderModel{
    
    _orderModel = orderModel;
    self.foodNameLabel.text = orderModel.fname;
    self.quantityLabel.text = [NSString stringWithFormat:@"x%@",orderModel.quantity];
    self.priceLabel.text = [NSString stringWithFormat:@"￥%.2f",[orderModel.check_price floatValue]];
}


#pragma mark - 懒加载
-(UILabel *)foodNameLabel{
    if(!_foodNameLabel){
        _foodNameLabel = [[UILabel alloc]init];
        _foodNameLabel.font = [UIFont systemFontOfSize:14.f];
        _foodNameLabel.textAlignment = NSTextAlignmentLeft;
        _foodNameLabel.textColor = [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
        _foodNameLabel.numberOfLines=0;
        _foodNameLabel.lineBreakMode = NSLineBreakByCharWrapping;
    }
    return _foodNameLabel;
}

-(UILabel *)quantityLabel{
    if (!_quantityLabel) {
        _quantityLabel = [[UILabel alloc]init];
        _quantityLabel.font = [UIFont systemFontOfSize:14.f];
        _quantityLabel.textAlignment = NSTextAlignmentLeft;
        _quantityLabel.textColor = [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
    }
    return _quantityLabel;
}

-(UILabel *)priceLabel{
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc]init];
        _priceLabel.font = [UIFont systemFontOfSize:14.f];
        _priceLabel.textAlignment = NSTextAlignmentRight;
        _priceLabel.textColor = [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
    }
    return _priceLabel;
}

-(UILabel *)lineLabel{
    if(!_lineLabel){
        _lineLabel = [[UILabel alloc]init];
        _lineLabel.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    }
    return _lineLabel;
}




- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
