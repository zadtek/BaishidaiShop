//
//  NewOrderFooterView.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/21.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderModel.h"



NS_ASSUME_NONNULL_BEGIN


@protocol OrderFooterDelegate <NSObject>

@optional

-(void)orderFooterCancel:(OrderModel*)orderModel;

-(void)orderFooterSend:(OrderModel*)orderModel;

-(void)callDelivery:(NSString*)phone;

@end



@interface NewOrderFooterView : UITableViewHeaderFooterView


@property(nonatomic,strong) OrderModel *orderModel;

@property(nonatomic,weak) id<OrderFooterDelegate> delegate;



@end

NS_ASSUME_NONNULL_END
