//
//  NewOrderFooterView.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/21.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "NewOrderFooterView.h"

@interface NewOrderFooterView ()

@property(nonatomic,strong) UILabel* otherTitleLabel;
@property(nonatomic,strong) UILabel* otherValueLabel;
@property(nonatomic,strong) UILabel* packageFeeTitleLabel;
@property(nonatomic,strong) UILabel* packageFeeValueLabel;

/**
 *  满减优惠
 */
@property(nonatomic,strong) UIView* fullCutView;
@property(nonatomic,strong) UILabel* cutIconLabel;
@property(nonatomic,strong) UILabel* cutTitleLabel;
@property(nonatomic,strong) UILabel* fullCutPriceLabel;

@property(nonatomic,strong) UILabel* lineLabel;
@property(nonatomic,strong) UILabel* sumPriceTitleLabel;
@property(nonatomic,strong) UILabel* sumPriceValueLabel;
@property(nonatomic,strong) UILabel* lineLabel2;


@property(nonatomic,strong) UIButton* cancelBtn;
@property(nonatomic,strong) UIButton* sendBtn;

@property(nonatomic,strong) UIView* bottomView;


@end

@implementation NewOrderFooterView

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self setUpUI];
    }
    return self;
}

#pragma mark -
-(void)setUpUI{
    
    [self addSubview:self.otherTitleLabel];
    [self addSubview:self.otherValueLabel];
    [self addSubview:self.packageFeeTitleLabel];
    [self addSubview:self.packageFeeValueLabel];
    [self addSubview:self.fullCutView];
    [self.fullCutView addSubview:self.cutIconLabel];
    [self.fullCutView addSubview:self.cutTitleLabel];
    [self.fullCutView addSubview:self.fullCutPriceLabel];
    [self addSubview:self.lineLabel];
    [self addSubview:self.sumPriceTitleLabel];
    [self addSubview:self.sumPriceValueLabel];
    [self addSubview:self.lineLabel2];
    [self addSubview:self.cancelBtn];
    [self addSubview:self.sendBtn];
    [self addSubview:self.bottomView];
    
    [self.otherTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.equalTo(self).offset(10);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(40);
    }];
    
    [self.otherValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.equalTo(self.otherTitleLabel);
        make.right.equalTo(self).offset(-10);
        make.left.equalTo(self.otherTitleLabel.mas_right);
    }];
    
    [self.packageFeeTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.otherTitleLabel);
        make.top.equalTo(self.otherTitleLabel.mas_bottom);
        make.height.mas_equalTo(30);
    }];
    
    [self.packageFeeValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.equalTo(self.packageFeeTitleLabel);
        make.right.equalTo(self.otherValueLabel);
        make.left.equalTo(self.packageFeeTitleLabel.mas_right);
    }];
    
    
    
    
    
    [self.fullCutView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.packageFeeTitleLabel.mas_bottom);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.mas_equalTo(20);
    }];
    
    
    [self.cutIconLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.fullCutView);
        make.left.equalTo(self.packageFeeTitleLabel);
        make.height.width.equalTo(self.fullCutView.mas_height).multipliedBy(0.8);
        
    }];
    
    [self.cutTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.fullCutView);
        make.left.equalTo(self.cutIconLabel.mas_right);
        make.width.equalTo(self.packageFeeTitleLabel);
        make.height.equalTo(self.fullCutView);
    }];
    
    [self.fullCutPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.fullCutView);
        make.left.equalTo(self.cutTitleLabel.mas_right);
        make.right.equalTo(self.packageFeeValueLabel);
        make.height.equalTo(self.fullCutView);
    }];
    
    [self.lineLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.fullCutView.mas_bottom);
        make.height.mas_equalTo(1);
        make.left.equalTo(self.otherTitleLabel);
        make.right.equalTo(self.otherValueLabel);
        
    }];
    
    
    [self.sumPriceTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineLabel.mas_bottom);
        make.left.right.equalTo(self.otherTitleLabel);
        make.height.mas_equalTo(40);
    }];
    
    [self.sumPriceValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.equalTo(self.sumPriceTitleLabel);
        make.right.equalTo(self.otherValueLabel);
        make.left.equalTo(self.sumPriceTitleLabel.mas_right);
    }];
    
    [self.lineLabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sumPriceTitleLabel.mas_bottom);
        make.height.mas_equalTo(1);
        make.left.equalTo(self.sumPriceTitleLabel);
        make.right.equalTo(self.sumPriceValueLabel);
        
    }];
    
    
    [self.sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineLabel2.mas_bottom).offset(10);
        make.right.equalTo(self).offset(-10);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(30);
    }];
    
    
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.width.height.equalTo(self.sendBtn);
        make.right.equalTo(self.sendBtn.mas_left).offset(-10);
    }];
    
    
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.mas_equalTo(10);
    }];
    
    
}


#pragma mark  - 代理方法
-(void)cancelTouch:(UIButton*)sender{
    if(self.delegate && [self.delegate respondsToSelector:@selector(orderFooterCancel:)]){
        [self.delegate orderFooterCancel:self.orderModel];
    }
}

-(void)sendTouch:(UIButton*)sender{
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(orderFooterSend:)]){
        [self.delegate orderFooterSend:self.orderModel];
    }
    
}


#pragma mark - orderModel
-(void)setOrderModel:(OrderModel *)orderModel{
    _orderModel = orderModel;
    double packageFee = [orderModel.packing_fee doubleValue];
    double totalPrice =[orderModel.total_amount doubleValue];
    double storeDiscount = [orderModel.discount doubleValue];
    self.packageFeeValueLabel.text = [NSString stringWithFormat:@"￥%.2f",packageFee];
    self.sumPriceValueLabel.text = [NSString stringWithFormat:@"￥%.2f",totalPrice];
    self.fullCutPriceLabel.text = [NSString stringWithFormat:@"-￥%.2f",storeDiscount];
    if(storeDiscount>0.00){
        self.fullCutView.hidden = NO;
    }else{
        self.fullCutView.hidden = YES;
    }
    
    self.cancelBtn.userInteractionEnabled = YES;
    
    
}


#pragma mark - 懒加载
-(UILabel *)otherTitleLabel{
    if(!_otherTitleLabel){
        _otherTitleLabel = [[UILabel alloc]init];
        _otherTitleLabel.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _otherTitleLabel.text = @"其他费用";
        _otherTitleLabel.font = kFontNameSize(17);
        
    }
    return _otherTitleLabel;
}

-(UILabel *)otherValueLabel{
    if(!_otherValueLabel){
        _otherValueLabel = [[UILabel alloc]init];
        _otherValueLabel.textAlignment = NSTextAlignmentRight;
        _otherValueLabel.font = kFontNameSize(17);
        
    }
    return _otherValueLabel;
}

-(UILabel *)packageFeeTitleLabel{
    if(!_packageFeeTitleLabel){
        _packageFeeTitleLabel = [[UILabel alloc]init];
        _packageFeeTitleLabel.font =  kFontNameSize(14);
        _packageFeeTitleLabel.textColor = [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
        _packageFeeTitleLabel.text = @"打包费";
    }
    return _packageFeeTitleLabel;
}

-(UILabel *)packageFeeValueLabel{
    if(!_packageFeeValueLabel){
        _packageFeeValueLabel = [[UILabel alloc]init];
        _packageFeeValueLabel.textColor= [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
        _packageFeeValueLabel.textAlignment = NSTextAlignmentRight;
        _packageFeeValueLabel.font = kFontNameSize(17);
        
    }
    return _packageFeeValueLabel;
}

-(UIView *)fullCutView{
    if(!_fullCutView){
        _fullCutView = [[UIView alloc]init];
        _fullCutView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _fullCutView;
}

-(UILabel *)cutIconLabel{
    if(!_cutIconLabel){
        _cutIconLabel = [[UILabel alloc]init];
        _cutIconLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"icon-full-cut"]];;
        _cutIconLabel.font =  kFontNameSize(14);
        _cutIconLabel.textColor = [UIColor whiteColor];
        _cutIconLabel.layer.masksToBounds = YES;
        _cutIconLabel.layer.cornerRadius = 3.f;
        _cutIconLabel.textAlignment = NSTextAlignmentCenter;
        _cutIconLabel.text =  @"减";
        _cutIconLabel.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _cutIconLabel;
}

-(UILabel *)cutTitleLabel{
    if(!_cutTitleLabel){
        _cutTitleLabel = [[UILabel alloc]init];
        _cutTitleLabel.font =  kFontNameSize(14);
        _cutTitleLabel.text =  @"满减优惠(商家承担):";
        _cutTitleLabel.textColor = [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
        _cutTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _cutTitleLabel;
}

-(UILabel *)fullCutPriceLabel{
    if(!_fullCutPriceLabel){
        _fullCutPriceLabel = [[UILabel alloc]init];
        _fullCutPriceLabel.text = @"￥0.00";
        _fullCutPriceLabel.textAlignment = NSTextAlignmentRight;
        _fullCutPriceLabel.textColor = [UIColor redColor];
        _fullCutPriceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _fullCutPriceLabel.font = kFontNameSize(17);
        
    }
    return _fullCutPriceLabel;
}
-(UILabel *)lineLabel{
    if(!_lineLabel){
        _lineLabel = [[UILabel alloc]init];
        _lineLabel.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    }
    return _lineLabel;
}

-(UILabel *)sumPriceTitleLabel{
    if(!_sumPriceTitleLabel){
        _sumPriceTitleLabel = [[UILabel alloc]init];
        _sumPriceTitleLabel.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _sumPriceTitleLabel.text = @"总计";
        _sumPriceTitleLabel.font = kFontNameSize(17);
        
    }
    return _sumPriceTitleLabel;
}

-(UILabel *)sumPriceValueLabel{
    if(!_sumPriceValueLabel){
        _sumPriceValueLabel = [[UILabel alloc]init];
        _sumPriceValueLabel.textColor = [UIColor colorWithRed:255/255.f green:77/255.f blue:77/255.f alpha:1.0];
        _sumPriceValueLabel.textAlignment = NSTextAlignmentRight;
        _sumPriceValueLabel.font = kFontNameSize(17);
        
    }
    return _sumPriceValueLabel;
}

-(UILabel *)lineLabel2{
    if(!_lineLabel2){
        _lineLabel2 = [[UILabel alloc]init];
        _lineLabel2.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    }
    return _lineLabel2;
}

-(UIButton *)cancelBtn{
    if(!_cancelBtn){
        _cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancelBtn setTitleColor:[UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0] forState:UIControlStateNormal];
        [_cancelBtn setTitle:@"拒绝接单" forState:UIControlStateNormal];
        _cancelBtn.layer.masksToBounds = YES;
        _cancelBtn.layer.cornerRadius = 5.f;
        _cancelBtn.layer.borderColor = [UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0].CGColor;
        _cancelBtn.layer.borderWidth = 1.f;
        _cancelBtn.titleLabel.font =  kFontNameSize(14);
        [_cancelBtn addTarget:self action:@selector(cancelTouch:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelBtn;
}

-(UIButton *)sendBtn{
    if(!_sendBtn){
        _sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sendBtn setTitleColor: [UIColor colorWithRed:255/255.f green:162/255.f blue:0/255.f alpha:1.0] forState:UIControlStateNormal];
        [_sendBtn setTitle: @"确认接单" forState:UIControlStateNormal];
        _sendBtn.layer.masksToBounds = YES;
        _sendBtn.layer.cornerRadius = 5.f;
        _sendBtn.layer.borderColor =  [UIColor colorWithRed:255/255.f green:162/255.f blue:0/255.f alpha:1.0].CGColor;
        _sendBtn.layer.borderWidth = 1.f;
        _sendBtn.titleLabel.font =  kFontNameSize(14);
        [_sendBtn addTarget:self action:@selector(sendTouch:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sendBtn;
    
}
-(UIView *)bottomView{
    if(!_bottomView){
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    }
    return _bottomView;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
