//
//  NewOrderHeaderView.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/21.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "NewOrderHeaderView.h"

@interface NewOrderHeaderView ()

@property(nonatomic,strong) UIView  * statusView;
@property(nonatomic,strong) UILabel * numberLabel;
@property(nonatomic,strong) UILabel * statusLabel;
@property(nonatomic,strong) UILabel * createDateLabel;
@property(nonatomic,strong) UILabel * lineLabel;
@property(nonatomic,strong) UILabel * payTypeLabel;
@property(nonatomic,strong) UIView  * userView;
@property(nonatomic,strong) UILabel * userNameLabel;
@property(nonatomic,strong) UILabel * addressLabel;
@property(nonatomic,strong) UILabel * remarkLabel;


@property(nonatomic,strong) UILabel * orderLabel;
@property(nonatomic,strong) UILabel * earningsLabel;


@property(nonatomic,strong) OrderModel* orderModel;

@end

@implementation NewOrderHeaderView

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self createSubviews];
        
    }
    return self;
}


#pragma mark -
-(void)createSubviews{
    
    [self addSubview:self.statusView];
    [self.statusView addSubview:self.numberLabel];
    [self.statusView addSubview:self.statusLabel];
    [self.statusView addSubview:self.createDateLabel];
    [self.statusView addSubview:self.lineLabel];
    [self.statusView addSubview:self.userNameLabel];
    [self.statusView addSubview:self.payTypeLabel];
    [self.statusView addSubview:self.orderLabel];
    [self.statusView addSubview:self.earningsLabel];
    
    
    [self addSubview:self.userView];
    [self.userView addSubview:self.addressLabel];
    [self.userView addSubview:self.remarkLabel];
    
  
    
    [self.statusView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.mas_equalTo(150);
    }];
    
    
    
    
    [self.userView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.statusView.mas_bottom);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.bottom.equalTo(self);
    }];
    
    
    
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.equalTo(self.statusView);
        make.left.equalTo(self.statusView).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(45.f, 45.f));
    }];
    
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.statusView).mas_offset(10);
        make.left.equalTo(self.numberLabel.mas_right).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH/4, 30.f));
    }];
    
    
    [self.createDateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.height.equalTo(self.statusLabel);
        make.left.equalTo(self.statusLabel.mas_right);
        make.right.equalTo(self.statusView).mas_offset(-10);
        
    }];
    
    [self.lineLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.statusLabel.mas_bottom);
        make.height.mas_equalTo(1);
        make.left.equalTo(self.statusLabel);
        make.right.equalTo(self.createDateLabel);
        
    }];
    
    
    [self.payTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineLabel.mas_bottom).mas_offset(10);
        make.height.mas_equalTo(25);
        make.right.equalTo(self.lineLabel);
        make.width.mas_equalTo(80);
        
    }];
    
    
    [self.userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.payTypeLabel);
        make.height.equalTo(self.payTypeLabel);
        make.left.equalTo(self.statusLabel);
        make.right.equalTo(self.payTypeLabel.mas_left);
        
    }];
    
    [self.orderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.payTypeLabel.mas_bottom).mas_offset(10);
        make.height.equalTo(self.payTypeLabel);
        make.right.equalTo(self.payTypeLabel);
        make.left.equalTo(self.statusLabel);
        
    }];
    
    [self.earningsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.orderLabel.mas_bottom).mas_offset(10);
        make.height.equalTo(self.orderLabel);
        make.right.equalTo(self.orderLabel);
        make.left.equalTo(self.orderLabel);
        
    }];
    
    
    
    
    UILabel *lineLabel1 = [[UILabel alloc]init];
    lineLabel1.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    [self.userView addSubview:lineLabel1];
    UILabel *lineLabel2 = [[UILabel alloc]init];
    lineLabel2.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    [self.userView addSubview:lineLabel2];
    UILabel *lineLabel3 = [[UILabel alloc]init];
    lineLabel3.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    [self.userView addSubview:lineLabel3];
    
    [lineLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userView);
        make.height.mas_equalTo(1);
        make.left.equalTo(self.numberLabel);
        make.right.equalTo(self.createDateLabel);
        
    }];
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineLabel1.mas_bottom);
        make.height.mas_equalTo(40);
        make.left.right.equalTo(lineLabel1);
    }];
    
    [lineLabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.addressLabel.mas_bottom);
        make.height.left.right.equalTo(lineLabel1);
        
    }];
    
    [self.remarkLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineLabel2.mas_bottom);
        make.height.left.right.equalTo(self.addressLabel);
    }];
    
    [lineLabel3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.userView.mas_bottom);
        make.height.left.right.equalTo(lineLabel1);
        
    }];

}

-(void)loadData:(OrderModel*)orderModel index:(NSInteger)index{
    
    
    _orderModel = orderModel;
    
    self.statusLabel.text = orderModel.status_txt;
    
    UIColor* color= [UIColor colorWithRed:255/255.f green:162/255.f blue:0/255.f alpha:1.0];
    
    self.numberLabel.textColor = color;
    self.numberLabel.layer.borderColor = color.CGColor;
    self.statusLabel.textColor = color;
    self.createDateLabel.textColor =color;
    self.lineLabel.backgroundColor = color;
    self.userNameLabel.textColor = color;
    self.payTypeLabel.textColor = color;
    self.payTypeLabel.layer.borderColor = color.CGColor;
    self.orderLabel.textColor = color;
    self.earningsLabel.textColor = color;
    

    self.numberLabel.text = [NSString stringWithFormat:@"%02ld",(long)index];
    self.createDateLabel.text = [NSString stringWithFormat:@"下单时间:%@",orderModel.add_time];
    self.payTypeLabel.text = orderModel.paytype;
    self.userNameLabel.text = [NSString stringWithFormat:@"%@ / %@",orderModel.uname,orderModel.phone];
    self.addressLabel.text = [NSString stringWithFormat:@"地址 : %@",orderModel.address1];
    NSString* mark =[WMHelper isEmptyOrNULLOrnil:orderModel.order_mark]?@"":orderModel.order_mark;
    NSString* str = [NSString stringWithFormat:@"备注 : %@",mark];
    NSMutableAttributedString* attributeStr = [[NSMutableAttributedString alloc]initWithString:str];
    [attributeStr addAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:255/255.f green:77/255.f blue:77/255.f alpha:1.0]} range:[str rangeOfString:mark]];
    self.remarkLabel.attributedText = attributeStr;
    
    
    self.orderLabel.text = [NSString stringWithFormat:@"订单号:%@",orderModel.order_sn];
    self.earningsLabel.text = [NSString stringWithFormat:@"本单收益:%@",orderModel.benifit];
    
    
}



-(void)labelTouchUpInside:(UITapGestureRecognizer *)sender{
    if(self.delegate && [self.delegate respondsToSelector:@selector(orderHeaderPhoneCall:)]){
        [self.delegate orderHeaderPhoneCall:self.orderModel];
    }
}

#pragma mark - 懒加载
-(UIView *)statusView{
    if(!_statusView){
        _statusView = [[UIView alloc]init];
        _statusView.backgroundColor = [UIColor whiteColor];
    }
    return _statusView;
}

-(UILabel *)numberLabel {
    if(!_numberLabel){
        _numberLabel = [[UILabel alloc]init];
        _numberLabel.font = kFontNameSize(24);
        _numberLabel.textAlignment = NSTextAlignmentCenter;
        _numberLabel.layer.masksToBounds = YES;
        _numberLabel.layer.cornerRadius = 5.f;
        _numberLabel.layer.borderWidth = 1.f;
    }
    return _numberLabel;
}

-(UILabel *)statusLabel{
    if(!_statusLabel){
        _statusLabel = [[UILabel alloc]init];
        _statusLabel.textAlignment = NSTextAlignmentLeft;
        _statusLabel.font =kFontNameSize(14);
    }
    return _statusLabel;
}



-(UILabel *)createDateLabel{
    if(!_createDateLabel){
        _createDateLabel = [[UILabel alloc]init];
        _createDateLabel.font = kFontNameSize(12);
        _createDateLabel.textAlignment = NSTextAlignmentRight;
    }
    return _createDateLabel;
}

-(UILabel *)lineLabel{
    if(!_lineLabel){
        _lineLabel = [[UILabel alloc]init];
        
    }
    return _lineLabel;
}
-(UILabel *)userNameLabel{
    if(!_userNameLabel){
        _userNameLabel = [[UILabel alloc]init];
        _userNameLabel.textAlignment = NSTextAlignmentLeft;
        _userNameLabel.font = kFontNameSize(14);
        
        
        _userNameLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTouchUpInside:)];
        [_userNameLabel addGestureRecognizer:tapgr];
        
        
    }
    return _userNameLabel;
}


-(UILabel *)payTypeLabel{
    if(!_payTypeLabel){
        _payTypeLabel = [[UILabel alloc]init];
        _payTypeLabel.textAlignment = NSTextAlignmentCenter;
        _payTypeLabel.font = kFontNameSize(14);
        _payTypeLabel.layer.masksToBounds = YES;
        _payTypeLabel.layer.cornerRadius = 5.f;
        _payTypeLabel.layer.borderWidth = 1.f;
    }
    return _payTypeLabel;
}

-(UIView *)userView{
    if(!_userView){
        _userView = [[UIView alloc]init];
        _userView.backgroundColor = [UIColor whiteColor];
    }
    return _userView;
}

-(UILabel *)addressLabel{
    if(!_addressLabel){
        _addressLabel = [[UILabel alloc]init];
        _addressLabel.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _addressLabel.font = kFontNameSize(14);
        _addressLabel.numberOfLines = 0;
        _addressLabel.lineBreakMode = NSLineBreakByWordWrapping;
    }
    return _addressLabel;
}

-(UILabel *)remarkLabel{
    if(!_remarkLabel){
        _remarkLabel = [[UILabel alloc]init];
        _remarkLabel.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _remarkLabel.font = kFontNameSize(13);
        _remarkLabel.numberOfLines = 0;
        _remarkLabel.lineBreakMode = NSLineBreakByCharWrapping;
    }
    return _remarkLabel;
}



-(UILabel *)orderLabel{
    if(!_orderLabel){
        _orderLabel = [[UILabel alloc]init];
        _orderLabel.textAlignment = NSTextAlignmentRight;
        _orderLabel.font = kFontNameSize(14);
    }
    return _orderLabel;
}

-(UILabel *)earningsLabel{
    if(!_earningsLabel){
        _earningsLabel = [[UILabel alloc]init];
        _earningsLabel.textAlignment = NSTextAlignmentRight;
        _earningsLabel.font = kFontNameSize(14);
    }
    return _earningsLabel;
}



@end
