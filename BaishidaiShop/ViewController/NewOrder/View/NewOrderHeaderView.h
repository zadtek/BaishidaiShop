//
//  NewOrderHeaderView.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/21.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderModel.h"


NS_ASSUME_NONNULL_BEGIN


@protocol OrderHeaderDelegate <NSObject>

@optional
-(void)orderHeaderPhoneCall:(OrderModel *)orderModel;

@end



@interface NewOrderHeaderView : UITableViewHeaderFooterView

-(void)loadData:(OrderModel*)orderModel index:(NSInteger)index;

@property(nonatomic,weak) id<OrderHeaderDelegate> delegate;


@end

NS_ASSUME_NONNULL_END
