//
//  OrderModel.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/21.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
//4=>'等待商家接单',          拒绝接单 确认接单
//11=>'商家已取消订单',
//13=>'商家已接单',   召唤骑手
//5=>'派单中',
//6=>'骑士前往商家取货中', 联系骑手
//7=>'已取货,配送中',   联系骑手
//8=>'已送达',    联系骑手
//9=>'待评价',    联系骑手
//10=>'已评价',    联系骑手

/**
 * 订单状态
 //4=>代表商家的新订单
 //5=>待抢单
 //6=>已抢单,取货中
 //7=>已取货,配送中
 //8=>已送达
 //9=>用户已确认
 //10=>已评价
 //11=>订单已经取消
 //12=>待确认
 //13=>商家已接单
 */
typedef enum : NSUInteger {
    
    OrderStatusUnPayed = 0,
    /**
     *  已发货
     */
    OrderStatusSended =1,
    /**
     *  已付款（处理中）
     */
    OrderStatusPayed =2,
    /**
     *  退款中
     */
    OrderStatusRefunding=3,
    /**
     *  新订单
     */
    OrderStatusNewsOrder =4,
    /**
     *  待抢单
     */
    OrderStatusQiangOrder=5,
    /**
     *  取货中
     */
    OrderStatusGetingGoods=6,
    /**
     *  配送中
     */
    OrderStatusOnTheWay=7,
    /**
     *已送达
     */
    OrderStatusAlredayGetGoods=8,
    /**
     *用户已确认
     */
    OrderStatusUsersMakeSure=9,
    /**
     *已评价
     */
    OrderStatusAlredayPingja=10,
    /**
     *订单已经取消
     */
    OrderStatusChancleOrder=11,
    /**
     *待确认
     */
    OrderStatusIngMakeSure=12,
    /**
     *商家已接单
     */
    OrderStatusAlredayJieDan=13,
    /**
     *待自取
     */
    OrderStatusWaitInvite=14,
    /**
     *用户已自取
     */
    OrderStatusAlredayInvite=15
    
    
    
} EnumOrderStatus;


@interface OrderModel : NSObject

@property(nonatomic,copy) NSString* add_time;

@property(nonatomic,copy) NSString* address1;

@property(nonatomic,copy) NSString* benifit;

@property(nonatomic,strong) NSMutableArray* delivery;
@property(nonatomic,copy) NSString* count;
@property(nonatomic,copy) NSString* phone;
@property(nonatomic,copy) NSString* realname;


@property(nonatomic,copy) NSString* discount;


@property(nonatomic,copy) NSString* emp_id;

@property(nonatomic,copy) NSString* food_amount;

@property(nonatomic,copy) NSString* food_check_amount;

@property(nonatomic,copy) NSString* goods_amount;

@property(nonatomic,strong) NSMutableArray* items;
@property(nonatomic,copy) NSString* check_price;
@property(nonatomic,copy) NSString* fname;
@property(nonatomic,copy) NSString* quantity;


@property(nonatomic,copy) NSString* order_id;

@property(nonatomic,copy) NSString* order_mark;


@property(nonatomic,copy) NSString* order_sn;


@property(nonatomic,copy) NSString* pack_fee;

@property(nonatomic,copy) NSString* packing_fee;

@property(nonatomic,copy) NSString* uname;
@property(nonatomic,copy) NSString* total_amount;
@property(nonatomic,copy) NSString* status;
@property(nonatomic,copy) NSString* ship_fee;
@property(nonatomic,copy) NSString* remark;

@property(nonatomic,copy) NSString* paytype;
@property(nonatomic,copy) NSString* status_txt;

/**
 *  是否展开默认为 NO (收缩)
 */
@property(nonatomic,assign) BOOL isExpansion;



@property(nonatomic,copy) NSString* status_val;


@end

NS_ASSUME_NONNULL_END
