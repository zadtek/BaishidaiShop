//
//  OrderModel.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/21.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "OrderModel.h"

@implementation OrderModel


+ (NSDictionary *)mj_objectClassInArray{
    
    return @{
             @"items" : @"OrderModel",
             @"delivery" : @"OrderModel"
             };//前边，是属性数组的名字，后边就是类名
}



@end
