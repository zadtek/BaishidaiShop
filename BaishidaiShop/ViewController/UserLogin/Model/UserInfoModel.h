//
//  UserInfoModel.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/23.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserInfoModel : NSObject


@property (nonatomic,copy) NSString *hxid;
@property (nonatomic,copy) NSString *is_real;
@property (nonatomic,copy) NSString *ru_id;
@property (nonatomic,copy) NSString *uid;

@property (nonatomic,copy) NSString *address;
@property (nonatomic,copy) NSString *balance;
@property (nonatomic,copy) NSString *close_time;


@property (nonatomic,copy) NSString *freeship_amount;
@property (nonatomic,copy) NSString *logo;
@property (nonatomic,copy) NSString *mobile;


@property (nonatomic,copy) NSString *user_sale;
@property (nonatomic,copy) NSString *open_time;
@property (nonatomic,copy) NSString *owner_name;
@property (nonatomic,copy) NSString *phone;

@property (nonatomic,copy) NSString *sale_full;
@property (nonatomic,copy) NSString *sale_sub;

@property (nonatomic,copy) NSString *send;
@property (nonatomic,copy) NSString *shop_sale;
@property (nonatomic,copy) NSString *site_name;
@property (nonatomic,copy) NSString *status;
@property (nonatomic,copy) NSString *status_remark;



@end

NS_ASSUME_NONNULL_END
