//
//  UserInfoModel.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/23.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "UserInfoModel.h"

@implementation UserInfoModel



+ (NSDictionary *)replacedKeyFromPropertyName {
    
    return @{
             
             @"user_sale":@"new_user_sale"
             
             };
    
    
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.hxid forKey:@"hxid"];
    [encoder encodeObject:self.is_real forKey:@"is_real"];
    [encoder encodeObject:self.ru_id forKey:@"ru_id"];
    [encoder encodeObject:self.uid forKey:@"uid"];
    
    [encoder encodeObject:self.address forKey:@"address"];
    [encoder encodeObject:self.balance forKey:@"balance"];
    [encoder encodeObject:self.close_time forKey:@"close_time"];
    
    [encoder encodeObject:self.freeship_amount forKey:@"freeship_amount"];
    [encoder encodeObject:self.logo forKey:@"logo"];
    [encoder encodeObject:self.mobile forKey:@"mobile"];
    
    [encoder encodeObject:self.user_sale forKey:@"user_sale"];
    [encoder encodeObject:self.open_time forKey:@"open_time"];
    [encoder encodeObject:self.owner_name forKey:@"owner_name"];
    [encoder encodeObject:self.phone forKey:@"phone"];
    
    
    [encoder encodeObject:self.sale_full forKey:@"sale_full"];
    [encoder encodeObject:self.sale_sub forKey:@"sale_sub"];
    [encoder encodeObject:self.send forKey:@"send"];
    [encoder encodeObject:self.shop_sale forKey:@"shop_sale"];
    [encoder encodeObject:self.site_name forKey:@"site_name"];
    [encoder encodeObject:self.status forKey:@"status"];
    [encoder encodeObject:self.status_remark forKey:@"status_remark"];
  
    
}


- (id)initWithCoder:(NSCoder *)decoder
{
    if(self = [super init])
    {
        self.hxid = [decoder decodeObjectForKey:@"hxid"];
        self.is_real = [decoder decodeObjectForKey:@"is_real"];
        self.ru_id = [decoder decodeObjectForKey:@"ru_id"];
        self.uid = [decoder decodeObjectForKey:@"uid"];
        self.address = [decoder decodeObjectForKey:@"address"];
        self.balance = [decoder decodeObjectForKey:@"balance"];
        self.close_time = [decoder decodeObjectForKey:@"close_time"];
        self.freeship_amount = [decoder decodeObjectForKey:@"freeship_amount"];
        self.logo = [decoder decodeObjectForKey:@"logo"];
        self.mobile = [decoder decodeObjectForKey:@"mobile"];
        self.user_sale = [decoder decodeObjectForKey:@"user_sale"];
        self.open_time = [decoder decodeObjectForKey:@"open_time"];
        self.owner_name = [decoder decodeObjectForKey:@"owner_name"];
        self.phone = [decoder decodeObjectForKey:@"phone"];
        self.sale_full = [decoder decodeObjectForKey:@"sale_full"];
        self.sale_sub = [decoder decodeObjectForKey:@"sale_sub"];
        
        
        self.send = [decoder decodeObjectForKey:@"send"];
        self.shop_sale = [decoder decodeObjectForKey:@"shop_sale"];
        self.site_name = [decoder decodeObjectForKey:@"site_name"];
        self.status = [decoder decodeObjectForKey:@"status"];
        
        self.status_remark = [decoder decodeObjectForKey:@"status_remark"];
      
    }
    return  self;
}




@end
