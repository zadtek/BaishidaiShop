//
//  LoginViewController.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/21.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "LoginViewController.h"
#import "MainTabBarViewController.h"
#import "JPUSHService.h"

@interface LoginViewController ()

@property(nonatomic,strong) UITextField* userNameTF;
@property(nonatomic,strong) UITextField* passwordTF;
@property(nonatomic,strong) UIButton* confirmBtn;



@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = @"登录";
    
    [self creatSubViews];

    
    
}

-(void)creatSubViews{
    
    
    [self.view addSubview:self.userNameTF];
    [self.view addSubview:self.passwordTF];
    [self.view addSubview:self.confirmBtn];
    
    [self.userNameTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).mas_offset(30);
        make.right.equalTo(self.view).mas_offset(-20);
        make.left.equalTo(self.view).mas_offset(20);
        make.height.mas_equalTo(45);

    }];

    
    [self.passwordTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userNameTF.mas_bottom).mas_offset(20);
        make.right.left.height.equalTo(self.userNameTF);
        
    }];
    
    
    [self.confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passwordTF.mas_bottom).mas_offset(20);
        make.right.left.height.equalTo(self.passwordTF);
        
    }];

}

#pragma mark - 点击登录
-(void)confirmBtnTouch:(UIButton*)sender{
    [self.view endEditing:YES];
    if([self checkForm]){
      
        
        [self showHUD];
        NSDictionary* arg = @{
                              @"ince":@"login",
                              @"username":self.userNameTF.text,
                              @"password":self.passwordTF.text
                              };

        [BSDNetTool PostURL:kHost  parameters:arg succeed:^(NSDictionary *data) {
            NSLog(@"登录 = %@",data);
            NSInteger flag = [[data objectForKey:@"ret"]integerValue];
            if(flag == 200){
                
                NSDictionary* emptyDict = [data objectForKey:@"data"];
                
                UserInfoModel *userInfoModel = [UserInfoModel mj_objectWithKeyValues:emptyDict];
                [[UserHandle shareInstance] setUserInfo:userInfoModel];
                [[UserHandle shareInstance] setUserId:userInfoModel.ru_id];

                [[UserHandle shareInstance] setloginState:YES];
                [[UserHandle shareInstance] synchronize];

                [self registerTagsWithAlias:userInfoModel.ru_id];

                
                [self hidHUD: @"登录成功" success:YES complete:^{
                    [self dismissViewControllerAnimated:YES completion:nil];
                    
                    [UIApplication sharedApplication].keyWindow.rootViewController = [[ MainTabBarViewController alloc] init];

                    
                }];
                
                
            }else{
                [self hidHUD:[data objectForKey:@"msg"]];
            }
            
        } failure:^(NSError *error) {
            [self hidHUD:@"网络异常" success:NO];
        }];
        
        
    }
}

#pragma mark  -
-(BOOL)checkForm{
    if([WMHelper isEmptyOrNULLOrnil:self.userNameTF.text]){
        [self alertHUD:@"用户名不能为空"];
        return NO;
    }else if([WMHelper isEmptyOrNULLOrnil:self.passwordTF.text]){
        [self alertHUD:@"密码不能为空"];
        return NO;
    }
    return YES;
    
}

-(void)registerTagsWithAlias:(NSString*)userID{
    
    [JPUSHService setAlias:userID completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
        NSLog(@"消息推送%tu,%@,%tu",iResCode,iAlias,seq);
        
        if(iResCode == 0){
            
        }else{
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"消息推送注失败,请退出后再登录!" preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertController animated:YES completion:nil];
        
        }
    } seq:0];
    
    
}


#pragma mark - 懒加载

-(UITextField *)userNameTF{
    if(!_userNameTF){
        UILabel* label = [[UILabel alloc]init];
        label.frame = CGRectMake(0, 0, 5, 45);
        _userNameTF = [[UITextField alloc]init];
        _userNameTF.placeholder = @"请输入您的用户名";
        _userNameTF.backgroundColor = [UIColor whiteColor];
        _userNameTF.borderStyle=UITextBorderStyleNone;
        _userNameTF.layer.borderColor =[UIColor colorWithRed:231/255.f green:231/255.f blue:231/255.f alpha:1.0].CGColor;
        _userNameTF.layer.borderWidth = 1.0f;
        _userNameTF.layer.masksToBounds = YES;
        _userNameTF.layer.cornerRadius = 5.f;
        _userNameTF.font = [UIFont systemFontOfSize:15.f];
        _userNameTF.leftView = label;
        _userNameTF.leftViewMode =UITextFieldViewModeAlways;
    }
    return _userNameTF;
}
-(UITextField *)passwordTF{
    if(!_passwordTF){
        UILabel* label = [[UILabel alloc]init];
        label.frame = CGRectMake(0, 0, 5, 45);
        _passwordTF = [[UITextField alloc]init];
        _passwordTF.placeholder = @"请输入您的登录密码";
        _passwordTF.backgroundColor = [UIColor whiteColor];
        _passwordTF.borderStyle=UITextBorderStyleNone;
        _passwordTF.layer.borderColor =[UIColor colorWithRed:231/255.f green:231/255.f blue:231/255.f alpha:1.0].CGColor;
        _passwordTF.layer.borderWidth = 1.0f;
        _passwordTF.layer.masksToBounds = YES;
        _passwordTF.layer.cornerRadius = 5.f;
        _passwordTF.font = [UIFont systemFontOfSize:15.f];
        _passwordTF.secureTextEntry = YES;
        _passwordTF.leftView = label;
        _passwordTF.leftViewMode =UITextFieldViewModeAlways;
    }
    return _passwordTF;
}



-(UIButton *)confirmBtn{
    if(!_confirmBtn){
        _confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _confirmBtn.backgroundColor = getColor(mainColor);
        [_confirmBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_confirmBtn setTitle:@"登录" forState:UIControlStateNormal];
        [_confirmBtn addTarget:self action:@selector(confirmBtnTouch:) forControlEvents:UIControlEventTouchUpInside];
        _confirmBtn.layer.masksToBounds = YES;
        _confirmBtn.layer.cornerRadius = 5.f;
//        _confirmBtn.layer.borderColor = [UIColor colorWithRed:231/255.f green:231/255.f blue:231/255.f alpha:1.0].CGColor;
        
    }
    return _confirmBtn;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
