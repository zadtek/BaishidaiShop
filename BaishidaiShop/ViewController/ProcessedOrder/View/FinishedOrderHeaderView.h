//
//  FinishedOrderHeaderView.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/21.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HandleOrderHeaderView.h"
#import "OrderModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface FinishedOrderHeaderView : UITableViewHeaderFooterView

-(void)loadData:(OrderModel*)orderModel index:(NSInteger)index;

@property(nonatomic,weak) id<HandleOrderHeaderDelegate> delegate;

@property (copy, nonatomic) void(^SummonKnightBlock)(void);

@property (copy, nonatomic) void(^CalPhoneBlock)(void);

@end

NS_ASSUME_NONNULL_END
