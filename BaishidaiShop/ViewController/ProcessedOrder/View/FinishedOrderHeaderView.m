//
//  FinishedOrderHeaderView.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/21.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "FinishedOrderHeaderView.h"

@interface FinishedOrderHeaderView ()
@property(nonatomic,strong) UIView* statusView;
@property(nonatomic,strong) UILabel* numberLabel;
@property(nonatomic,strong) UILabel* statusLabel;
@property(nonatomic,strong) UIImageView* iconArrow;
@property(nonatomic,strong) UILabel* createDateLabel;
@property(nonatomic,strong) UILabel* lineLabel;
@property(nonatomic,strong) UILabel* payTypeLabel;
@property(nonatomic,strong) UILabel* userNameLabel;
@property(nonatomic,strong) UIButton *zhqsBtn;// 召唤骑士

@property(nonatomic,strong) OrderModel* orderModel;

@end


@implementation FinishedOrderHeaderView


- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
        [self createSubviews];
        
        UITapGestureRecognizer *tapgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureTouch:)];
        [self addGestureRecognizer:tapgr];
        
    }
    return self;
}

#pragma mark  - createSubviews
-(void)createSubviews{
    
    [self addSubview:self.statusView];
    [self.statusView addSubview:self.numberLabel];
    [self.statusView addSubview:self.statusLabel];
    [self.statusView addSubview:self.iconArrow];
    [self.statusView addSubview:self.createDateLabel];
    [self.statusView addSubview:self.lineLabel];
    [self.statusView addSubview:self.userNameLabel];
    [self.statusView addSubview:self.zhqsBtn];
    
    self.statusView.backgroundColor = [UIColor whiteColor];
    
    
    [self.statusView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.bottom.equalTo(self).offset(-10);
        
    }];
    
    
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.equalTo(self.statusView);
        make.left.equalTo(self.statusView).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(45.f, 45.f));
    }];
    
    
    
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.statusView).mas_offset(10);
        make.left.equalTo(self.numberLabel.mas_right).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH/3, 30.f));
    }];
    
    [self.iconArrow mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.equalTo(self.statusLabel);
        make.left.equalTo(self.statusLabel.mas_right).mas_offset(5);
        make.size.mas_equalTo(CGSizeMake(10.f, 10.f));
    }];
    
    [self.createDateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.height.equalTo(self.statusLabel);
        make.left.equalTo(self.iconArrow.mas_right);
        make.right.equalTo(self.statusView).mas_offset(-10);
        
    }];
    
    [self.lineLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.statusLabel.mas_bottom);
        make.height.mas_equalTo(1);
        make.left.equalTo(self.statusLabel);
        make.right.equalTo(self.createDateLabel);
        
    }];
    
    
    [self.zhqsBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineLabel.mas_bottom).mas_offset(10);
        make.height.mas_equalTo(25);
        make.right.equalTo(self.lineLabel);
        make.width.mas_equalTo(80);
        
    }];
    
    
    [self.userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.zhqsBtn);
        make.height.equalTo(self.zhqsBtn);
        make.left.equalTo(self.statusLabel);
        make.right.equalTo(self.zhqsBtn.mas_left);
        
    }];
    
    
}



-(void)loadData:(OrderModel*)orderModel index:(NSInteger)index{
    _orderModel = orderModel;
    UIColor* color;
    NSString* imageName;
    NSString* btnName;

    _zhqsBtn.hidden = YES;
    
    
    self.statusLabel.text = orderModel.status_txt;

    
    NSInteger flag = [orderModel.status integerValue];
    switch (flag) {
            
        case OrderStatusUnPayed://未付款
        {
            color = [UIColor grayColor];
            imageName = @"icon-arrow-cancel";
        }
            break;
            
        case OrderStatusNewsOrder://新订单
        {
            color = [UIColor colorWithRed:255/255.f green:162/255.f blue:0/255.f alpha:1.0];
            imageName = @"icon-arrow-finished";
        }
            break;
        case OrderStatusQiangOrder://待抢单
        {
            color = [UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0];
            imageName = @"icon-arrow-finished";
        }
            break;
        case OrderStatusGetingGoods://取货中
        {
            color = [UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0];
            imageName = @"icon-arrow-finished";
            _zhqsBtn.hidden = NO;
            btnName = @"联系骑手";
        }
            break;
        case OrderStatusOnTheWay://配送中
        {
            color = [UIColor colorWithRed:135/255.f green:135/255.f blue:135/255.f alpha:1.0];
            imageName = @"icon-arrow-cancel";
            _zhqsBtn.hidden = NO;
            btnName = @"联系骑手";
        }
            break;
            
        case OrderStatusAlredayGetGoods://已送达
        {
            color = [UIColor colorWithRed:135/255.f green:135/255.f blue:135/255.f alpha:1.0];
            imageName = @"icon-arrow-cancel";
            _zhqsBtn.hidden = NO;
            btnName = @"联系骑手";
        }
            break;
            
        case OrderStatusUsersMakeSure://已确认
        {
            color = [UIColor redColor];
            imageName = @"icon-arrow-ing";
            _zhqsBtn.hidden = NO;
            btnName = @"联系骑手";
        }
            break;
        case OrderStatusAlredayPingja://已评价
        {
            color = [UIColor redColor];
            imageName = @"icon-arrow-ing";
            _zhqsBtn.hidden = NO;
            btnName = @"联系骑手";
        }
            break;
        case OrderStatusChancleOrder://已取消
        {
            color = [UIColor redColor];
            imageName = @"icon-arrow-ing";
        }
            break;
        case OrderStatusIngMakeSure://待确认
        {
            color = [UIColor redColor];
            imageName = @"icon-arrow-ing";
        }
            break;
        case OrderStatusAlredayJieDan://已接单
        {
            color = [UIColor redColor];
            imageName = @"icon-arrow-ing";
            _zhqsBtn.hidden = NO;
            btnName = @"召唤骑士";
        }
            break;
            
        case OrderStatusWaitInvite://待自取
        {
            color = [UIColor grayColor];
            imageName = @"icon-arrow-cancel";
            _zhqsBtn.hidden = NO;
            btnName = @"完成自取";
        }
            break;
       
        case OrderStatusAlredayInvite://用户已自取
        {
            color = [UIColor grayColor];
            imageName = @"icon-arrow-cancel";
        }
            break;
            
            
            
        default:
            break;
    }
    self.numberLabel.textColor = color;
    self.numberLabel.layer.borderColor = color.CGColor;
    self.statusLabel.textColor = color;
    self.createDateLabel.textColor =color;
    self.lineLabel.backgroundColor = color;
    self.userNameLabel.textColor = color;

    
    self.zhqsBtn.layer.borderColor = color.CGColor;
    [self.zhqsBtn setTitleColor:color forState:UIControlStateNormal];
    [_zhqsBtn setTitle:btnName forState:UIControlStateNormal];

    
    if(flag == OrderStatusPayed){
        self.iconArrow.hidden = YES;
    }else{
        self.iconArrow.hidden = NO;
        [self.iconArrow setImage:[UIImage imageNamed:imageName]];
    }

    
    if(orderModel.isExpansion){
        [UIView animateWithDuration:0.5 animations:^{
            self.iconArrow.transform = CGAffineTransformMakeRotation(M_PI/2);
        } completion:^(BOOL finished) {
            
        }];
        
    }else {
        [UIView animateWithDuration:0.5 animations:^{
            self.iconArrow.transform = CGAffineTransformMakeRotation(0);
        } completion:^(BOOL finished) {
            
        }];
        
    }
    self.numberLabel.text = [NSString stringWithFormat:@"%02ld",index];
    self.createDateLabel.text = [NSString stringWithFormat:@"下单时间:%@",orderModel.add_time];
    self.userNameLabel.text = [NSString stringWithFormat:@"%@ / %@",orderModel.uname,orderModel.phone];
    
}


#pragma mark - 点击事件
-(void)gestureTouch:(UIGestureRecognizer*)sender{
    if(self.delegate && [self.delegate respondsToSelector:@selector(handleOrderHeader:)]){
        [self.delegate handleOrderHeader:self.orderModel];
    }
}
-(void)callPhone{
    
    if (self.CalPhoneBlock) {
        self.CalPhoneBlock();
    }
    
}

#pragma mark - 懒加载
-(UIView *)statusView{
    if(!_statusView){
        _statusView = [[UIView alloc]init];
        _statusView.backgroundColor = [UIColor whiteColor];
    }
    return _statusView;
}

-(UILabel *)numberLabel {
    if(!_numberLabel){
        _numberLabel = [[UILabel alloc]init];
        _numberLabel.font = kFontNameSize(24);
        _numberLabel.textAlignment = NSTextAlignmentCenter;
        _numberLabel.layer.masksToBounds = YES;
        _numberLabel.layer.cornerRadius = 5.f;
        _numberLabel.layer.borderWidth = 1.f;
    }
    return _numberLabel;
}

-(UILabel *)statusLabel{
    if(!_statusLabel){
        _statusLabel = [[UILabel alloc]init];
        _statusLabel.textAlignment = NSTextAlignmentLeft;
        _statusLabel.font = kFontNameSize(13);
    }
    return _statusLabel;
}

-(UIImageView *)iconArrow{
    if(!_iconArrow){
        _iconArrow = [[UIImageView alloc]init];
        _iconArrow.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _iconArrow;
}

-(UILabel *)createDateLabel{
    if(!_createDateLabel){
        _createDateLabel = [[UILabel alloc]init];
        _createDateLabel.font = kFontNameSize(12);
        _createDateLabel.textAlignment = NSTextAlignmentRight;
    }
    return _createDateLabel;
}

-(UILabel *)lineLabel{
    if(!_lineLabel){
        _lineLabel = [[UILabel alloc]init];
        
    }
    return _lineLabel;
}
-(UILabel *)userNameLabel{
    if(!_userNameLabel){
        _userNameLabel = [[UILabel alloc]init];
        
        _userNameLabel.textAlignment = NSTextAlignmentLeft;
        _userNameLabel.font = kFontNameSize(14);
        
        _userNameLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callPhone)];
        [_userNameLabel addGestureRecognizer:tapgr];
        
    }
    return _userNameLabel;
}
-(UILabel *)payTypeLabel{
    if(!_payTypeLabel){
        _payTypeLabel = [[UILabel alloc]init];
        
        _payTypeLabel.textAlignment = NSTextAlignmentCenter;
        _payTypeLabel.font = kFontNameSize(14);
        _payTypeLabel.layer.masksToBounds = YES;
        _payTypeLabel.layer.cornerRadius = 5.f;
        
        _payTypeLabel.layer.borderWidth = 1.f;
    }
    return _payTypeLabel;
}
- (UIButton *)zhqsBtn
{
    if (!_zhqsBtn) {
        _zhqsBtn = [[UIButton alloc]init];
        _zhqsBtn.backgroundColor = [UIColor clearColor];
        _zhqsBtn.titleLabel.font = kFontNameSize(14);
        _zhqsBtn.layer.cornerRadius = 5;
        _zhqsBtn.layer.borderWidth = 1.f;
        _zhqsBtn.layer.masksToBounds = YES;
        _zhqsBtn.hidden = YES;
        [_zhqsBtn addTarget:self action:@selector(SummonKnightButton) forControlEvents:UIControlEventTouchUpInside];
    }
    return _zhqsBtn;
}

-(void)SummonKnightButton{
    
    if (self.SummonKnightBlock) {
        self.SummonKnightBlock();
    }
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
