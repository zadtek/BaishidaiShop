//
//  FinishedOrderFooterView.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/21.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "FinishedOrderFooterView.h"

@interface FinishedOrderFooterView ()

@property(nonatomic,strong) UILabel* otherTitleLabel;
@property(nonatomic,strong) UILabel* otherValueLabel;
@property(nonatomic,strong) UILabel* packageFeeTitleLabel;
@property(nonatomic,strong) UILabel* packageFeeValueLabel;
/**
 *  满减优惠
 */
@property(nonatomic,strong) UIView* fullCutView;
@property(nonatomic,strong) UILabel* cutIconLabel;
@property(nonatomic,strong) UILabel* cutTitleLabel;
@property(nonatomic,strong) UILabel* fullCutPriceLabel;

@property(nonatomic,strong) UILabel* lineLabel;
@property(nonatomic,strong) UILabel* lineLabel1;
@property(nonatomic,strong) UILabel* lineLabel2;
@property(nonatomic,strong) UILabel* lineLabel3;

@property(nonatomic,strong) UILabel* sumPriceTitleLabel;
@property(nonatomic,strong) UILabel* sumPriceValueLabel;


@property(nonatomic,strong) UILabel* typeTitleLabel;
@property(nonatomic,strong) UILabel* typeValueLabel;

@property(nonatomic,strong) UILabel* orderTitleLabel;
@property(nonatomic,strong) UILabel* orderValueLabel;


@property(nonatomic,strong) UILabel* earningsTitleLabel;
@property(nonatomic,strong) UILabel* earningsValueLabel;


/**
 *  快递员信息
 */
@property(nonatomic,strong) UIView* deliveryView;
@property(nonatomic,strong) UIView* deliveryLineView;
@property(nonatomic,strong) UIButton* deliveryBtn;
@property(nonatomic,strong) UILabel* deliveryCountLabel;



@property(nonatomic,strong) UIView* bottomView;

@end

@implementation FinishedOrderFooterView



- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if (self) {
         self.contentView.backgroundColor = [UIColor whiteColor];
        [self createSubviews];
    }
    return self;
}


#pragma mark -
-(void)createSubviews{

    [self addSubview:self.otherTitleLabel];
    [self addSubview:self.otherValueLabel];
    [self addSubview:self.packageFeeTitleLabel];
    [self addSubview:self.packageFeeValueLabel];
    [self addSubview:self.fullCutView];
    [self.fullCutView addSubview:self.cutIconLabel];
    [self.fullCutView addSubview:self.cutTitleLabel];
    [self.fullCutView addSubview:self.fullCutPriceLabel];
    [self addSubview:self.lineLabel];
    [self addSubview:self.lineLabel1];
    [self addSubview:self.lineLabel2];
    [self addSubview:self.lineLabel3];
    
    [self addSubview:self.typeTitleLabel];
    [self addSubview:self.typeValueLabel];
    
    [self addSubview:self.orderTitleLabel];
    [self addSubview:self.orderValueLabel];
    
    [self addSubview:self.earningsTitleLabel];
    [self addSubview:self.earningsValueLabel];
    
    
    [self addSubview:self.sumPriceTitleLabel];
    [self addSubview:self.sumPriceValueLabel];
    [self addSubview:self.bottomView];
    
    [self addSubview:self.deliveryView];
    [self.deliveryView addSubview:self.deliveryLineView];
    [self.deliveryView addSubview:self.deliveryBtn];
    [self.deliveryView addSubview:self.deliveryCountLabel];
    
    
    
    
    
    
    [self.otherTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.equalTo(self).offset(10);
        make.width.mas_equalTo(150);
        make.height.mas_equalTo(35);
    }];
    
    [self.otherValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.equalTo(self.otherTitleLabel);
        make.right.equalTo(self).offset(-10);
        make.left.equalTo(self.otherTitleLabel.mas_right);
    }];
    
    [self.packageFeeTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.otherTitleLabel);
        make.top.equalTo(self.otherTitleLabel.mas_bottom);
        make.height.mas_equalTo(20);
    }];
    
    [self.packageFeeValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.equalTo(self.packageFeeTitleLabel);
        make.right.equalTo(self.otherValueLabel);
        make.left.equalTo(self.packageFeeTitleLabel.mas_right);
    }];
    
    
    
    
    
    [self.fullCutView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.packageFeeTitleLabel.mas_bottom);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.mas_equalTo(20);
    }];
    
    
    [self.cutIconLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.fullCutView);
        make.left.equalTo(self.packageFeeTitleLabel);
        make.height.width.equalTo(self.fullCutView.mas_height).multipliedBy(0.8);
        
    }];
    
    [self.cutTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.fullCutView);
        make.left.equalTo(self.cutIconLabel.mas_right);
        make.width.equalTo(self.packageFeeTitleLabel);
        make.height.equalTo(self.fullCutView);
    }];
    
    [self.fullCutPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.fullCutView);
        make.left.equalTo(self.cutTitleLabel.mas_right);
        make.right.equalTo(self.packageFeeValueLabel);
        make.height.equalTo(self.fullCutView);
    }];
    
    [self.lineLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.fullCutView.mas_bottom);
        make.height.mas_equalTo(1);
        make.left.equalTo(self.otherTitleLabel);
        make.right.equalTo(self.otherValueLabel);
        
    }];
    
    [self.typeTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineLabel.mas_bottom);
        make.left.right.equalTo(self.otherTitleLabel);
        make.height.mas_equalTo(40);
    }];
    
    [self.typeValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.equalTo(self.typeTitleLabel);
        make.right.equalTo(self.otherValueLabel);
        make.left.equalTo(self.typeTitleLabel.mas_right);
    }];
    
    [self.lineLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.typeTitleLabel.mas_bottom);
        make.height.mas_equalTo(1);
        make.left.equalTo(self.typeTitleLabel);
        make.right.equalTo(self.typeValueLabel);
        
    }];
    
    
    [self.orderTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineLabel1.mas_bottom);
        make.left.right.height.equalTo(self.typeTitleLabel);
    }];
    
    [self.orderValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.equalTo(self.orderTitleLabel);
        make.right.equalTo(self.typeValueLabel);
        make.left.equalTo(self.typeTitleLabel.mas_right);
    }];
    
    [self.lineLabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.orderTitleLabel.mas_bottom);
        make.height.mas_equalTo(1);
        make.left.equalTo(self.orderTitleLabel);
        make.right.equalTo(self.orderValueLabel);
        
    }];
    
    
    [self.earningsTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineLabel2.mas_bottom);
        make.left.right.height.equalTo(self.typeTitleLabel);
    }];
    
    [self.earningsValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.equalTo(self.earningsTitleLabel);
        make.right.equalTo(self.orderValueLabel);
        make.left.equalTo(self.earningsTitleLabel.mas_right);
    }];
    
    [self.lineLabel3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.earningsTitleLabel.mas_bottom);
        make.height.mas_equalTo(1);
        make.left.equalTo(self.earningsTitleLabel);
        make.right.equalTo(self.earningsValueLabel);
        
    }];
    
    [self.sumPriceTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineLabel3.mas_bottom);
        make.left.right.height.equalTo(self.typeTitleLabel);
    }];
    
    [self.sumPriceValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.height.equalTo(self.sumPriceTitleLabel);
        make.right.equalTo(self.orderValueLabel);
        make.left.equalTo(self.sumPriceTitleLabel.mas_right);
    }];
    
    
    
    [self.deliveryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sumPriceTitleLabel.mas_bottom);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.mas_equalTo(40);
    }];
    
    
    [self.deliveryLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.deliveryView);
        make.height.mas_equalTo(1);
        make.left.equalTo(self.lineLabel2);
        make.right.equalTo(self.lineLabel2);
        
    }];
    
    
    [self.deliveryBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.deliveryLineView.mas_bottom);
        make.width.equalTo(self.deliveryView).multipliedBy(0.6);
        make.left.equalTo(self.deliveryView).offset(10);
        make.bottom.equalTo(self.deliveryView);
    }];
    
    
    [self.deliveryCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.deliveryLineView.mas_bottom);
        make.right.equalTo(self.deliveryView).offset(-10);
        make.left.equalTo(self.deliveryBtn.mas_right);
        make.bottom.equalTo(self.deliveryView);
    }];
    
    
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.mas_equalTo(10);
    }];
    
    
}

-(void)setOrderModel:(OrderModel *)orderModel{
    
    
    _orderModel = orderModel;
    double packageFee = [orderModel.packing_fee doubleValue];
    double totalPrice =[orderModel.total_amount doubleValue];
    double storeDiscount = [orderModel.discount doubleValue];
    
    self.packageFeeValueLabel.text = [NSString stringWithFormat:@"￥%.2f",packageFee];
    self.sumPriceValueLabel.text = [NSString stringWithFormat:@"￥%.2f",totalPrice];
    self.fullCutPriceLabel.text = [NSString stringWithFormat:@"-￥%.2f",storeDiscount];
    if(storeDiscount>0.00){
        self.fullCutView.hidden = NO;
    }else{
        self.fullCutView.hidden = YES;
    }
    
    
    self.orderValueLabel.text = [NSString stringWithFormat:@"%@",orderModel.order_sn];
    
    self.earningsValueLabel.text = [NSString stringWithFormat:@"%@",orderModel.benifit];
    
    NSString *type;
    if([orderModel.paytype isEqualToString:@"0"]){
        type = @"现金支付";
    }else if ([orderModel.paytype isEqualToString:@"1"]){
        type = @"支付宝支付";
    }
    else if ([orderModel.paytype isEqualToString:@"2"]){
        type = @"微信支付";
    }
    else if ([orderModel.paytype isEqualToString:@"3"]){
        type = @"手机支付宝";
    }else{
        type =  @"其它支付";
    }
    
    self.typeValueLabel.text = [NSString stringWithFormat:@"%@",type];
    
    
    NSArray *deliveryArr = orderModel.delivery;
    OrderModel *deliveryModel = deliveryArr.firstObject;
    if(deliveryArr.count != 0){
        self.deliveryView.hidden = NO;
        NSString* str = [NSString stringWithFormat: @"配送员: %@ / %@",deliveryModel.realname,deliveryModel.phone];
        NSMutableAttributedString* attributeStr = [[NSMutableAttributedString alloc]initWithString: str];
        [attributeStr addAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0]} range:NSMakeRange(0, str.length)];
        [attributeStr addAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:30/255.f green:147/255.f blue:238/255.f alpha:1.0],NSFontAttributeName:[UIFont systemFontOfSize:15.f]} range:[str rangeOfString:deliveryModel.phone]];
        [self.deliveryBtn setAttributedTitle:attributeStr forState:UIControlStateNormal];
        str = [NSString stringWithFormat: @"当前已接:%@单",deliveryModel.count];
        attributeStr = [[NSMutableAttributedString alloc]initWithString: str];
        [attributeStr addAttributes:@{NSForegroundColorAttributeName:[UIColor redColor],NSFontAttributeName:[UIFont systemFontOfSize:15.f]} range:[str rangeOfString:deliveryModel.count]];
        self.deliveryCountLabel.attributedText = attributeStr;
    }else{
        self.deliveryView.hidden = YES;
    }
    
    
}


#pragma mark - 点击事件
-(void)callEmpty:(UIButton *)sender{
    
    OrderModel *deliveryModel = self.orderModel.delivery.firstObject;
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(handleFooterCall:)]){
        [self.delegate handleFooterCall:deliveryModel.phone];
    }
}

#pragma mark - 懒加载
-(UILabel *)otherTitleLabel{
    if(!_otherTitleLabel){
        _otherTitleLabel = [[UILabel alloc]init];
        _otherTitleLabel.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _otherTitleLabel.text = @"其他费用";
        _otherTitleLabel.font = kFontNameSize(17);

    }
    return _otherTitleLabel;
}

-(UILabel *)otherValueLabel{
    if(!_otherValueLabel){
        _otherValueLabel = [[UILabel alloc]init];
        _otherValueLabel.textAlignment = NSTextAlignmentRight;
        _otherValueLabel.font = kFontNameSize(17);

    }
    return _otherValueLabel;
}

-(UILabel *)packageFeeTitleLabel{
    if(!_packageFeeTitleLabel){
        _packageFeeTitleLabel = [[UILabel alloc]init];
        _packageFeeTitleLabel.font = kFontNameSize(14);
        _packageFeeTitleLabel.textColor = [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
        _packageFeeTitleLabel.text = @"打包费";
    }
    return _packageFeeTitleLabel;
}

-(UILabel *)packageFeeValueLabel{
    if(!_packageFeeValueLabel){
        _packageFeeValueLabel = [[UILabel alloc]init];
        _packageFeeValueLabel.textColor= [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
        _packageFeeValueLabel.textAlignment = NSTextAlignmentRight;
        _packageFeeValueLabel.font = kFontNameSize(17);

    }
    return _packageFeeValueLabel;
}


-(UIView *)fullCutView{
    if(!_fullCutView){
        _fullCutView = [[UIView alloc]init];
        _fullCutView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _fullCutView;
}

-(UILabel *)cutIconLabel{
    if(!_cutIconLabel){
        _cutIconLabel = [[UILabel alloc]init];
        _cutIconLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"icon-full-cut"]];;
        _cutIconLabel.font = kFontNameSize(13);
        _cutIconLabel.textColor = [UIColor whiteColor];
        _cutIconLabel.layer.masksToBounds = YES;
        _cutIconLabel.layer.cornerRadius = 3.f;
        _cutIconLabel.textAlignment = NSTextAlignmentCenter;
        _cutIconLabel.text =  @"减";
        _cutIconLabel.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _cutIconLabel;
}

-(UILabel *)cutTitleLabel{
    if(!_cutTitleLabel){
        _cutTitleLabel = [[UILabel alloc]init];
        _cutTitleLabel.font = kFontNameSize(13);
        _cutTitleLabel.text =  @"满减优惠(商家承担):";
        _cutTitleLabel.textColor = [UIColor colorWithRed:149/255.f green:149/255.f blue:149/255.f alpha:1.0];
        _cutTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _cutTitleLabel;
}

-(UILabel *)fullCutPriceLabel{
    if(!_fullCutPriceLabel){
        _fullCutPriceLabel = [[UILabel alloc]init];
        _fullCutPriceLabel.text = @"￥0.00";
        _fullCutPriceLabel.textAlignment = NSTextAlignmentRight;
        _fullCutPriceLabel.textColor = [UIColor redColor];
        _fullCutPriceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _fullCutPriceLabel.font = kFontNameSize(17);

    }
    return _fullCutPriceLabel;
}


-(UILabel *)lineLabel{
    if(!_lineLabel){
        _lineLabel = [[UILabel alloc]init];
        _lineLabel.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    }
    return _lineLabel;
}
-(UILabel *)lineLabel1{
    if(!_lineLabel1){
        _lineLabel1 = [[UILabel alloc]init];
        _lineLabel1.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    }
    return _lineLabel1;
}

-(UILabel *)lineLabel2{
    if(!_lineLabel2){
        _lineLabel2 = [[UILabel alloc]init];
        _lineLabel2.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    }
    return _lineLabel2;
}

-(UILabel *)lineLabel3{
    if(!_lineLabel3){
        _lineLabel3 = [[UILabel alloc]init];
        _lineLabel3.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    }
    return _lineLabel3;
}


-(UILabel *)sumPriceTitleLabel{
    if(!_sumPriceTitleLabel){
        _sumPriceTitleLabel = [[UILabel alloc]init];
        _sumPriceTitleLabel.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _sumPriceTitleLabel.text = @"总计";
        _sumPriceTitleLabel.font = kFontNameSize(17);

    }
    return _sumPriceTitleLabel;
}

-(UILabel *)sumPriceValueLabel{
    if(!_sumPriceValueLabel){
        _sumPriceValueLabel = [[UILabel alloc]init];
        _sumPriceValueLabel.textColor = [UIColor colorWithRed:255/255.f green:77/255.f blue:77/255.f alpha:1.0];
        _sumPriceValueLabel.textAlignment = NSTextAlignmentRight;
        _sumPriceValueLabel.font = kFontNameSize(17);

    }
    return _sumPriceValueLabel;
}
-(UIView *)bottomView{
    if(!_bottomView){
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    }
    return _bottomView;
}


-(UILabel *)typeTitleLabel{
    if(!_typeTitleLabel){
        _typeTitleLabel = [[UILabel alloc]init];
        _typeTitleLabel.font = kFontNameSize(15);
        _typeTitleLabel.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _typeTitleLabel.text = @"支付方式";
    }
    return _typeTitleLabel;
}

-(UILabel *)typeValueLabel{
    if(!_typeValueLabel){
        _typeValueLabel = [[UILabel alloc]init];
        _typeValueLabel.textColor= [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _typeValueLabel.font = kFontNameSize(15);
        _typeValueLabel.textAlignment = NSTextAlignmentRight;
    }
    return _typeValueLabel;
}


-(UILabel *)orderTitleLabel{
    if(!_orderTitleLabel){
        _orderTitleLabel = [[UILabel alloc]init];
        _orderTitleLabel.font = kFontNameSize(15);
        _orderTitleLabel.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _orderTitleLabel.text = @"订单号";
    }
    return _orderTitleLabel;
}

-(UILabel *)orderValueLabel{
    if(!_orderValueLabel){
        _orderValueLabel = [[UILabel alloc]init];
        _orderValueLabel.textColor= [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _orderValueLabel.font = kFontNameSize(15);
        _orderValueLabel.textAlignment = NSTextAlignmentRight;
    }
    return _orderValueLabel;
}


-(UILabel *)earningsTitleLabel{
    if(!_earningsTitleLabel){
        _earningsTitleLabel = [[UILabel alloc]init];
        _earningsTitleLabel.font = kFontNameSize(15);
        _earningsTitleLabel.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _earningsTitleLabel.text = @"本单收益";
    }
    return _earningsTitleLabel;
}

-(UILabel *)earningsValueLabel{
    if(!_earningsValueLabel){
        _earningsValueLabel = [[UILabel alloc]init];
        _earningsValueLabel.textColor= [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1.0];
        _earningsValueLabel.font = kFontNameSize(15);
        _earningsValueLabel.textAlignment = NSTextAlignmentRight;
    }
    return _earningsValueLabel;
}

-(UIView *)deliveryView{
    if(!_deliveryView){
        _deliveryView = [[UIView alloc]init];
    }
    return _deliveryView;
}

-(UIView *)deliveryLineView{
    if(!_deliveryLineView){
        _deliveryLineView = [[UIView alloc]init];
        _deliveryLineView.backgroundColor = [UIColor colorWithRed:209/255.f green:212/255.f blue:216/255.f alpha:1.0];
    }
    return _deliveryLineView;
}


-(UIButton *)deliveryBtn{
    if(!_deliveryBtn){
        _deliveryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deliveryBtn setTitleColor:[UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0] forState:UIControlStateNormal] ;
        _deliveryBtn.titleLabel.font = kFontNameSize(14);
        [_deliveryBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [_deliveryBtn addTarget:self action:@selector(callEmpty:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deliveryBtn;
}

-(UILabel *)deliveryCountLabel{
    if(!_deliveryCountLabel){
        _deliveryCountLabel = [[UILabel alloc]init];
        _deliveryCountLabel.textColor = [UIColor colorWithRed:41/255.f green:189/255.f blue:221/255.f alpha:1.0];
        _deliveryCountLabel.textAlignment = NSTextAlignmentRight;
        _deliveryCountLabel.font = kFontNameSize(14);
    }
    return _deliveryCountLabel;
}


@end
