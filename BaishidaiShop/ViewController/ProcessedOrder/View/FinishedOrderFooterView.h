//
//  FinishedOrderFooterView.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/21.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderModel.h"


NS_ASSUME_NONNULL_BEGIN

@protocol HandleFooterCallDelegate <NSObject>

@optional

-(void)handleFooterCall:(NSString*)phoneNum;

@end

@interface FinishedOrderFooterView : UITableViewHeaderFooterView


@property(nonatomic,weak) id<HandleFooterCallDelegate> delegate;

@property(nonatomic,strong) OrderModel* orderModel;


@end

NS_ASSUME_NONNULL_END
