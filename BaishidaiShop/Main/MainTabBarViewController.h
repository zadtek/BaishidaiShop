//
//  MainTabBarViewController.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/18.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MainTabBarViewController : UITabBarController

-(void)jumpHome;

@end

NS_ASSUME_NONNULL_END
