//
//  BaseViewController.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/18.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface BaseViewController : UIViewController


@property (nonatomic, strong) MBProgressHUD * progressHUD;

/**
 *  显示大菊花
 */
-(void)showprogressHUD;

/**
 *  隐藏大菊花
 */
-(void)hiddenProgressHUD;

- (void)back;
@end
