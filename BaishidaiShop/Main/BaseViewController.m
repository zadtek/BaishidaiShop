//
//  BaseViewController.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/18.
//  Copyright © 2019年 sy. All rights reserved.
//
#import "BaseViewController.h"


@interface BaseViewController ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic,copy) void(^selectPhotoHandle)(UIImage *img);

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //布局 不延伸
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.automaticallyAdjustsScrollViewInsets = NO;

    [self initNavigationBar];
    
    
}
-(void)initNavigationBar
{
    
    //返回按钮
    if (self.navigationController.viewControllers.count > 1) {
        
        UIBarButtonItem *backButton =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_return_b"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
        self.navigationItem.leftBarButtonItem = backButton;
    }
    
}

- (void)back {
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (MBProgressHUD *)progressHUD {
    if (!_progressHUD) {
        _progressHUD = [[MBProgressHUD alloc]initWithView:self.view];
    }
    return _progressHUD;
}

/**
 *  显示大菊花
 */
-(void)showprogressHUD{
    self.tabBarController.view.userInteractionEnabled = NO;
    self.navigationController.view.userInteractionEnabled = NO;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.navigationController.view addSubview:self.progressHUD];
    [self.progressHUD showAnimated:YES];
}

/**
 *  隐藏大菊花
 */
-(void)hiddenProgressHUD{
    self.tabBarController.view.userInteractionEnabled = YES;
    self.navigationController.view.userInteractionEnabled = YES;
    [self.progressHUD hideAnimated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self.progressHUD removeFromSuperview];
}


@end
