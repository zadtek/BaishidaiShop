//
//  MainTabBarViewController.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/18.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "MainTabBarViewController.h"
#import "MainNavgationViewController.h"

#import "NewOrderViewController.h"
#import "ProcessedOrderViewController.h"
#import "StoreMangeViewController.h"



@interface MainTabBarViewController ()<UITabBarControllerDelegate>

@end

@implementation MainTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.delegate = self;
    [self.tabBar setTranslucent:NO];
//    self.tabBar.tintColor = theme_navigation_color;
    
    
    [self addchildViewVc:[[NewOrderViewController alloc]init] withImagName:@"tab-new-order-default" withSelectedImagName:@"tab-new-order-enter" andTitle:@"新订单"];
    [self addchildViewVc:[[ProcessedOrderViewController alloc]init] withImagName:@"tab-processed-order-default" withSelectedImagName:@"tab-processed-order-enter"  andTitle:@"已处理订单"];
    [self addchildViewVc:[[StoreMangeViewController alloc]init] withImagName:@"tab-store-default" withSelectedImagName:@"tab-store-enter" andTitle:@"门店管理"];
    
    
    
}
//添加子控制器方法
- (void)addchildViewVc:(UIViewController *)vc withImagName:(NSString *)imgName withSelectedImagName:(NSString *)selectedName andTitle:(NSString *)titile{
    
    //设置标题
    vc.title = titile;
    //设置图片
    vc.tabBarItem.image  = [[UIImage imageNamed:imgName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    vc.tabBarItem.selectedImage = [[UIImage imageNamed:selectedName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    //设置选中状态item背景颜色//8fc31f
    [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:getColor(mainColor),NSFontAttributeName:kFontNameSize(15)} forState:UIControlStateSelected];
    
    
    //添加子控制器
    MainNavgationViewController *navVc = [[MainNavgationViewController alloc] initWithRootViewController:vc];
    [self addChildViewController:navVc];
}



#pragma mark - ******* 处理远程推送过来的消息 *******
-(void)jumpHome{
    
    
    self.selectedIndex = 0;
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
