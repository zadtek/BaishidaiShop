//
//  POIRequest.h
//  poi-gaode
//
//  Created by W_L on 2018/10/19.
//  Copyright © 2018 W_L. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
//#import <AMapLocationKit/AMapLocationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>


typedef void(^ReturnData)(NSArray *arr);


@interface POIRequest : NSObject
- (void)searchRequestCLLocationCoordinate2D:(CLLocationCoordinate2D )cLLocationCoordinate2D
                                   keyWords:(NSString *)keyWords
                                   cityName:(NSString *)cityName
                                 complation:(ReturnData)complation;


@property (copy, nonatomic)ReturnData returnData;

@end

