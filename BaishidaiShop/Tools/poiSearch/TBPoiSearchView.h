//
//  TBPoiSearchView.h
//  poi-gaode
//
//  Created by W_L on 2018/10/24.
//  Copyright © 2018 W_L. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TBPoiSearchViewDelegate <NSObject>
//MARK: 点击城市按钮
- (void)tb_TapCityBtnhHandler:(UIButton *)button;

- (BOOL)tb_SearchBarShouldBeginEditing:(UISearchBar *)searchBar;

- (void)tb_SearchBarCancelButtonClicked:(UISearchBar *)searchBar;

- (void)tb_SearchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText;

- (void)tb_SearchBarSearchButtonClicked:(UISearchBar *)searchBar;


@end

@interface TBPoiSearchView : UIView

//搜索框
@property (strong, nonatomic) UISearchBar *searchBar;
//城市按钮
@property (strong, nonatomic) UIButton *cityBtn;

@property (weak, nonatomic)id <TBPoiSearchViewDelegate> delegate;



@end


