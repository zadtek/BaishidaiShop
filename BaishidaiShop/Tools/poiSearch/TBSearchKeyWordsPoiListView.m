//
//  TBSearchKeyWordsPoiListView.m
//  poi-gaode
//
//  Created by W_L on 2018/10/19.
//  Copyright © 2018 W_L. All rights reserved.
//

#import "TBSearchKeyWordsPoiListView.h"
@interface TBSearchKeyWordsPoiListView  ()<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) UITableView *mainTableView;

@property (strong, nonatomic) NSArray *dataSource;

@end
@implementation TBSearchKeyWordsPoiListView



- (instancetype)initWithFrame:(CGRect)frame{
    
    self =   [super initWithFrame:frame];
    
    if (self) {
        
        [self addSubview:self.mainTableView];
    }
    
    return self;
}




#pragma mark -- lazy
-(UITableView *)mainTableView{
    if (!_mainTableView) {
        _mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame)) style:(UITableViewStyleGrouped)];
        _mainTableView.backgroundColor = [UIColor whiteColor];
        _mainTableView.delegate = self;
        
        _mainTableView.dataSource = self;
        
        _mainTableView.estimatedRowHeight = 33;
        
        _mainTableView.rowHeight = UITableViewAutomaticDimension;
        
        //        _mainTableView.bounces = NO;
        
        if (@available(iOS 11.0, *)) {
            
            _mainTableView.estimatedSectionFooterHeight = 0.1;
            
            _mainTableView.estimatedSectionHeaderHeight = 0.1;
        }
        
    }
    
    
    return _mainTableView;
}



#pragma mark -- tableViewDelegate and datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataSource.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleSubtitle) reuseIdentifier:@"UITableViewCell"];
    }
    
    NSDictionary *dic = self.dataSource[indexPath.row];
    
    cell.textLabel.text = dic[@"poi_name"];
    cell.detailTextLabel.text = dic[@"poi_address"];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dic = self.dataSource[indexPath.row];

    
    if ([self.delegate respondsToSelector:@selector(tb_KeyWordsDidselectPoiList:)]) {
        [self.delegate tb_KeyWordsDidselectPoiList:dic];
    }
    
}


- (void)updataDataSource:(NSArray *)dataSource{
    
    self.dataSource = dataSource;
    
    [self.mainTableView reloadData];
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
