//
//  UIViewController+Category.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/18.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIViewController (Category)<MBProgressHUDDelegate>

@property(nonatomic,strong) MBProgressHUD* HUD;

@property(nonatomic,assign) BOOL firstLoad;

-(void)showHUD;
-(void)showHUD:(NSString*)message;

-(void)hidHUD;
-(void)hidHUD:(NSString*)message;
-(void)hidHUD:(NSString*)message success:(BOOL)success;
-(void)hidHUD:(NSString*)message success:(BOOL)success complete:(dispatch_block_t) complete;

-(void)alertHUD:(NSString*)message;
-(void)alertHUD:(NSString*)message complete:(dispatch_block_t) complete;
-(void)alertHUD:(NSString *)message delay:(NSTimeInterval)delay;

-(void)checkNetWorkState:(void (^)(AFNetworkReachabilityStatus netWorkStatus))complete;

@end
