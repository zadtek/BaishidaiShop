//
//  UIImage+Color.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/18.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 *  根据颜色返回图片
 */
@interface UIImage (Color)
+ (UIImage*) imageWithColor:(UIColor*)color;
@end
