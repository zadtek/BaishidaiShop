//
//  CustomAlertView.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/18.
//  Copyright © 2019年 sy. All rights reserved.
//
#import <UIKit/UIKit.h>

typedef void(^AlertResult)(NSString *fullPrice,NSString *cutPrice);

typedef void(^AlertTextViewResult)(NSString *textStr);

@interface CustomAlertView : UIView


@property (nonatomic,copy) AlertResult resultIndex;

@property (nonatomic,copy) AlertTextViewResult textBlock;


- (instancetype)initAlert;


- (instancetype)initTextViewAlert;


- (void)showAlertView;



@end
