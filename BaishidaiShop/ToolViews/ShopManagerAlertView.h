//
//  ShopManagerAlertView.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/18.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,ShopManagerAlert) {

    ShopManagerTitle = 1,
    ShopManagerPrice = 2

};

typedef void(^ShopAlertResult)(NSString *changeText);


@interface ShopManagerAlertView : UIView

@property (nonatomic,copy) ShopAlertResult resultIndex;

- (instancetype)initAlertWithType:(ShopManagerAlert)type withText:(NSString *)text;

- (void)showAlertView;

@end

NS_ASSUME_NONNULL_END
