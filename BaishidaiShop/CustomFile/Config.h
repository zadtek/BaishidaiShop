//
//  Config.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/18.
//  Copyright © 2019年 sy. All rights reserved.


#import <UIKit/UIKit.h>


#pragma mark --------log


/**
 * 一般分割线颜色
 */
extern NSString *dividerColor;

/**
 * 字体颜色
 */
extern NSString *textColor;

/**
 * 一般背景颜色
 */
extern NSString *bgColor;

/**
 *  文字按钮  白色
 */
extern NSString *whiteColor;

/**
 *  主颜色
 */
extern NSString *mainColor;

/**
 *  文字\按钮  蓝色
 */
extern NSString *blueColor;

/**
 *  文字\按钮  黑色
 */
extern NSString *blackColor;

/**
 *  背景绿色
 */
extern NSString *greenBackColor;

extern NSString *lightTextColor;


@interface Config : NSObject

UIColor* getColor(NSString * hexColor);

- (BOOL) isBlankString:(NSString *)string;
// 拼接字符串
- (NSString *) connectionString:(NSString *)connectionUrl;


@end
