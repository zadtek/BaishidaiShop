//
//  UserHandle.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/23.
//  Copyright © 2019年 sy. All rights reserved.
//

#import "UserHandle.h"
#import "UserInfoModel.h"

@implementation UserHandle


static UserHandle *toolsHandle = nil;


+ (UserHandle *)shareInstance
{
    if (toolsHandle == nil) {
        
        toolsHandle = [[UserHandle alloc] init];
        
    }
    
    return toolsHandle;
}
//同步
- (void)synchronize
{
    [[NSUserDefaults standardUserDefaults] synchronize];
}
//设置用户信息
- (void)setloginState:(BOOL)isLogin
{
    [[NSUserDefaults standardUserDefaults] setBool:isLogin forKey:kLoginState];
}
-(void)setUserId:(NSString *)userId
{
    [[NSUserDefaults standardUserDefaults] setObject:userId forKey:kUserid];
}

//获取用户信息
- (BOOL)loginState
{
    return     [[NSUserDefaults standardUserDefaults] boolForKey:kLoginState];
    
}

- (NSString *)userId
{
    return     [[NSUserDefaults standardUserDefaults] objectForKey:kUserid];
    
}


-(void)setUserInfo:(UserInfoModel *)userInfo{
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:userInfo];
    
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:kUserInfo];
    
}

-(UserInfoModel *)userInfo{
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:kUserInfo];
    
    UserInfoModel *userInfo = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    return userInfo;
    
}


/**
 *  清除所有的存储本地的数据
 */
- (void)clearAllUserDefaultsData
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary* defaults = [userDefaults dictionaryRepresentation];
    for (id key in defaults) {
        if (![key isEqualToString:VERSION_INFO_CURRENT]) {
            [userDefaults removeObjectForKey:key];
            [userDefaults synchronize];
        } else {
            NSLog(@"%@",[userDefaults objectForKey:key]);
        }
    }
    
}


@end
