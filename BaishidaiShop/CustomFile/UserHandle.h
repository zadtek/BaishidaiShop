//
//  UserHandle.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/23.
//  Copyright © 2019年 sy. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UserInfoModel;


NS_ASSUME_NONNULL_BEGIN

@interface UserHandle : NSObject

@property (nonatomic,strong) UserInfoModel *userInfo;


+ (UserHandle *)shareInstance;

//同步
- (void)synchronize;
//设置用户信息
- (void)setloginState:(BOOL)isLogin;//是否登录
- (void)setUserId:(NSString *)userId;


//获取用户信息
- (BOOL)loginState;
- (NSString *)userId;




-(void)setUserInfo:(UserInfoModel *)userInfo;

-(UserInfoModel *)userInfo;

-(void)clearAllUserDefaultsData;



@end

NS_ASSUME_NONNULL_END
