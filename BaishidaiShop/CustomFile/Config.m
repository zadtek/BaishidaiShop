//
//  Config.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/18.
//  Copyright © 2019年 sy. All rights reserved.


#import "Config.h"

/**
 * 一般分割线颜色
 */
NSString *dividerColor = @"e5e5e5";

/**
 * 字体颜色
 */
NSString *textColor = @"979797";

/**
 * 一般背景颜色
 */
NSString *bgColor = @"e5e5e5";

/**
 *  文字按钮  白色
 */
NSString *whiteColor = @"ffffff";

/**
 *  主颜色
 */
NSString *mainColor = @"2788cc";

/**
 *  文字\按钮  蓝色
 */
NSString *blueColor = @"2995eb";

/**
 *  文字\按钮  黑色
 */
NSString *blackColor = @"000000";

/**
 *  背景绿色
 */
NSString *greenBackColor = @"2788cc";

NSString *lightTextColor = @"#c0c0c0";

@implementation Config

UIColor* getColor(NSString * hexColor)
{
    unsigned int red, green, blue;
    NSRange range;
    range.length = 2;
    
    range.location = 0;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&red];
    range.location = 2;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&green];
    range.location = 4;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&blue];
    
    return [UIColor colorWithRed:(float)(red/255.0f) green:(float)(green/255.0f) blue:(float)(blue/255.0f) alpha:1.0f];
}


- (BOOL) isBlankString:(NSString *)string
{
    if (string == nil || string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}

@end

