//
//  Pager.m
//  BaishidaiShop
//
//  Created by mac book on 2019/1/18.
//  Copyright © 2019年 sy. All rights reserved.


#import "Pager.h"

@implementation Pager
-(instancetype)init{
    self = [super init];
    if(self){
        _pageIndex = 1;
        _pageSize = 35;
    }
    return self;
}

-(NSMutableArray *)arrayData{
    if(_arrayData ==nil)
        _arrayData = [NSMutableArray new];
    return _arrayData;
}
@end
