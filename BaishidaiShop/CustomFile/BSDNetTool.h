//
//  BSDNetTool.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/18.
//  Copyright © 2019年 sy. All rights reserved.


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>



@interface BSDNetTool : NSObject


+ (BSDNetTool *)sharedUtil;/** * iOS自带网络请求框架 */



/** * AFN get数据请求 */
+(void)GetURL:(NSString *)URLString
parameters:(NSDictionary*)parameters
   succeed:(void (^)(NSDictionary* data))succeed
   failure:(void (^)(NSError *error))failure;




/** * AFN post数据请求 */
+(void)PostURL:(NSString *)URLString
    parameters:(NSDictionary*)parameters
       succeed:(void (^)(NSDictionary* data))succeed
       failure:(void (^)(NSError *error))failure;




/** * 上传单张图片 */
+(void)requestAFURL:(NSString *)URLString
         parameters:(id)parameters
          imageData:(NSData *)imageData
            succeed:(void (^)(NSDictionary* data))succeed
            failure:(void (^)(NSError *error))failure;



/** * 上传多张图片 */
+(void)requestAFURL:(NSString *)URLString
         parameters:(id)parameters
     imageDataArray:(NSArray *)imageDataArray
            succeed:(void (^)(NSDictionary* data))succeed
            failure:(void (^)(NSError *error))failure;


/** * 上传文件 */
+(void)requestAFURL:(NSString *)URLString
         parameters:(id)parameters
           fileData:(NSData *)fileData
            succeed:(void (^)(id))succeed
            failure:(void (^)(NSError *error))failure;



+ (void)requestCancel;


@end
