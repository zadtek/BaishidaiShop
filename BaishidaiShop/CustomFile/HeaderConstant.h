//
//  HeaderConstant.h
//  BaishidaiShop
//
//  Created by mac book on 2019/1/18.
//  Copyright © 2019年 sy. All rights reserved.


#ifndef HeaderConstant_h
#define HeaderConstant_h


#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define StatusBar_HEIGHT ([[UIApplication sharedApplication] statusBarFrame].size.height)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define SCREEN [[UIScreen mainScreen]bounds]

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)



//----------------------系统----------------------------

// 是否iPad
#define isPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
// 是否iPad
#define someThing (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)? ipad: iphone

//获取系统版本
#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]

#define ISIOS7 [[[UIDevice currentDevice]systemVersion] doubleValue] >= 7.0


//检查系统版本
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


// 颜色
#define kMyColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]



//字体
#define kFontNameSize(fontNameSize)      [UIFont fontWithName:@"PingFang-SC-Regular" size:fontNameSize]



#define Strong @property (nonatomic, strong)
#define Assign @property (nonatomic, assign)
#define Copy @property (nonatomic, copy)
//显示msg
#define ShowMessage(msg) [self.view makeToast:msg duration:1 position:CSToastPositionCenter]
#define ShowViewMessage(msg) [self  makeToast:msg duration:1 position:CSToastPositionCenter]


// 顶部宏 iPhone X：状态栏44  导航栏44  其他：状态栏20  导航栏44
#define STATUS_HEIGHT (SCREEN_HEIGHT == 812.0 ? 44 : 20)
// 顶部宏 状态栏44或者20  导航栏44
#define SafeAreaTopHeight (SCREEN_HEIGHT == 812.0 ? 88 : 64)
// 底部宏 （没有tarbar的情况） iPhone X 下面多34高度
#define SafeAreaBottomHeight (SCREEN_HEIGHT == 812.0 ? 34 : 0)
// 底部宏 （有tarbar的情况）iPhone X ：tarbar总高度 49 + 34   其他：49
#define TABBAR_HEIGHT (SCREEN_HEIGHT == 812.0 ? 83 : 49)
/**
 根据状态栏判断 iPhone X
 导航栏和状态栏的总高度
 */
#define NAVGATIONBAR_HEIGHT (CGRectGetHeight([UIApplication sharedApplication].statusBarFrame) == 20 ? 64 : 88)

//5S宽高比例

#define ScreenBounds [UIScreen mainScreen].bounds
#define WIDTH_5S_SCALE 320.0 * [UIScreen mainScreen].bounds.size.width
#define HEIGHT_5S_SCALE 568.0 * [UIScreen mainScreen].bounds.size.height

#define WIDTH_6S_SCALE 375.0 * [UIScreen mainScreen].bounds.size.width
#define HEIGHT_6S_SCALE 667.0 * [UIScreen mainScreen].bounds.size.height
//弱应用
#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;



#define AMap_ApiKey @"2a096ab1adc173f27d131ec4b98e00be" //高德

#define kHost @"http://baishidaishop.jipeisong.cn/mobile/Api/index"




#define kLoginState   @"isLogin"
#define kUserName     @"username"
#define kPassword     @"password"
#define kUserid   @"userid"
#define kUserInfo   @"userInfo"
#define kUserPhone     @"userphone"
#define VERSION_INFO_CURRENT @"currentversion"





#endif /* HeaderConstant_h */
